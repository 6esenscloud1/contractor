$('.owl-carousel').owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    autoplay:true,
    responsive:{
        0:{
            items:1
        },
        768:{
            items:2
        },
        992:{
            items:3
        }
    }
});