<div class="row mt-3">
    <div class="col-md-6">
        <!-- title At Field -->
        <div class="form-group">
            {!! Form::label('name', 'Nom & Prénom:') !!}
            <p>{{ $team->name }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- title At Field -->
        <div class="form-group">
            {!! Form::label('post', 'Post Occupé:') !!}
            <p>{{ $team->post }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-4">
        <!-- title At Field -->
        <div class="form-group">
            {!! Form::label('link_linkedin', 'Lien LinkedIn:') !!}
            <p>{{ $team->link_linkedin }}</p>
        </div>
    </div>
    <div class="col-md-4">
        <!-- title At Field -->
        <div class="form-group">
            {!! Form::label('link_facebook', 'Lien Facebook:') !!}
            <p>{{ $team->link_facebook }}</p>
        </div>
    </div>
    <div class="col-md-4">
        <!-- title At Field -->
        <div class="form-group">
            {!! Form::label('link_twitter', 'Lien Facebook:') !!}
            <p>{{ $team->link_twitter }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <!-- subtitle At Field -->
        <div class="form-group">
            {!! Form::label('image', 'Image:') !!}
            <p><img src="{{ asset('/storage/images/' . $team->image) }}" alt="{{ $team->name }}" width="250" height="250" /></p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Date de Création:') !!}
            <p>{{ $team->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Updated At Field -->
        <div class="form-group">
            {!! Form::label('updated_at', 'Date de Modification:') !!}
            <p>{{ $team->updated_at }}</p>
        </div>
    </div>
</div>





