{{-- @extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('teams.index') !!}">Team</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Team</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($team, ['route' => ['teams.update', $team->id], 'method' => 'patch']) !!}

                              @include('teams.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection --}}

@extends('Admin_BackOffice.Admin_layouts.master')

@section('title')
    Notre Equipe | {{ config('app.name') }}
@endsection

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Modifier un employé</h4>
                            <p class="card-category">Information sur un employé</p>
                        </div>
                        <div class="card-body">
                            {!! Form::model($team, ['route' => ['teams.update', $team->id], 'method' => 'patch', 'files' => true]) !!}

                              @include('teams.fields')

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
