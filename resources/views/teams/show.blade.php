{{-- @extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('teams.index') }}">Team</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('teams.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('teams.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection --}}


@extends('Admin_BackOffice.Admin_layouts.master')

@section('title')
   Notre Equipe | {{ config('app.name') }}
@endsection


@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Détails équipe</h4>
                            <p class="card-category">Information sur l'équipe</p>
                        </div>
                        <div class="card-body">
                            {{-- <div class="row"> --}}
                                @include('teams.show_fields')
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <a href="{{ route('teams.edit', [$team->id]) }}" class="btn btn-primary pull-right">Modifier</a>
                                    </div>
                                    <div class="form-group col-sm-2">
                                        <a href="{{ route('teams.index') }}" class="btn btn-primary pull-right">Retour</a>
                                    </div>
                                </div>
                            {{-- </div> --}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
