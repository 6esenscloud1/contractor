<div class="row mt-3">
    <div class="col-md-6">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('name', 'Nom & Prénom:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('post', 'Poste Occupé:') !!}
            {!! Form::text('post', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            <strong>{!! Form::label('image', 'Image de Présentation:') !!}</strong>
            {!! Form::file('image', ['class' => 'form-control', 'onchange' => 'loadFile(event)']) !!}
        </div>
    </div>
</div>
@if ($team ?? '')
    <div class="row">
        <div class="col-md-6 output">
            <div class="form-group">
                <img src="{{ asset('/storage/images/' . $team->image) }}" alt="{{ $team->name }}" width="250" height="250" />
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class="form-group">
                <img style="width: 50%; height: 50%; border: none" id="output" />
            </div>
        </div>
    </div>
@endif
<div class="row mt-3">
    {{--<div class="col-md-6">
        <div class="form-group">
            <input type="checkbox" name="is_free" value="0">
        </div>
    </div>--}}
</div>

<script>
    var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById('output');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };

    function showPrice(val) {
        if (val == 'NON') {
            document.getElementById("entry_free").style.display = 'block';
        } else {
            document.getElementById("entry_free").style.display = 'none';
        }
    }

</script>


<div class="row mt-3">
    <div class="col-md-4">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('link_linkedin', 'Lien LinkendIn:') !!}
            {!! Form::text('link_linkedin', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('link_facebook', 'Lien Facebook:') !!}
            {!! Form::text('link_facebook', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('link_twitter', 'Lien Twitter:') !!}
            {!! Form::text('link_twitter', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('teams.index') }}" class="btn btn-secondary">Annulé</a>
</div>
