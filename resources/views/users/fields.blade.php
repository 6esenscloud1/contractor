<div class="row mt-3">
    <div class="col-md-6">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('name', 'Nom:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <!-- Email Field -->
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
       <!-- Password Field -->
        <div class="form-group">
            {!! Form::label('password', 'Mot de passe:') !!}
            {!! Form::text('password', null, ['class' => 'form-control']) !!}
        </div> 
    </div>

    <div class="col-md-6">
        <!-- Role Field -->
        <div class="form-group">
            {!! Form::label('role_id', 'Collections :') !!}
            <select name="role_id" class="custom-select input-style placeholder-style" id="cluster_id">
                <option value="">Choisir un rôle</option>
                @foreach($role as $roles)
                    <option value="{{$roles->id}}">{{$roles->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('users.index') }}" class="btn btn-secondary">Annulé</a>
</div>
