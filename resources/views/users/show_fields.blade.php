<div class="row mt-3">
    <div class="col-md-4">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('name', 'Nom:') !!}
            <p>{{ $user->name }}</p>
        </div>
    </div>
    <div class="col-md-4">
        <!-- Slug Field -->
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            <p>{{ $user->email }}</p>
        </div>
    </div>
    <div class="col-md-4">
        <!-- Role_id Field -->
        <div class="form-group">
            {!! Form::label('role_id', 'Rôle:') !!}
            <p>{{ $user->getRoleAttribute()->name ?? '---' }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Created At:') !!}
            <p>{{ $user->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Updated At Field -->
        <div class="form-group">
            {!! Form::label('updated_at', 'Updated At:') !!}
            <p>{{ $user->updated_at }}</p>
        </div>
    </div>
</div>




