@extends('Customer_FrontOffice.LayoutFrontOffice.masterPage')

@section('title')
    Nous-contacter | {{ config('app.name') }}
@endsection

@section('content')

        <header class="header-front">
        <div class="row mr-0" id="row-banner">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4 main-breadcrumb" >
                @if( request()->is('contact') )
                    <p class="breadcrumb-title">contact</p>
                    <ul class="breadcrumb-ul">
                        <li class="breadcrumb-li">
                            <a  href="{{url('/')}}">accueil</a>
                        </li>
                        <div class="vertical-breadcrumb"></div>
                        <li class="breadcrumb-li breadcrumb-li-here">
                            <a  href="#" class="here">Contact</a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
    </header>

    <div class="container" id="contact">
       {{-- <div class="title-page" >
            <h2>Contactez-nous</h2>
            <p class="slug">Et posons la première pierre de votre projet</p>
        </div> --}}
        <div class="row mt-3">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4">
                <div class="title-metier">
                    <h1>Contactez-nous</h1>
                    <p class="text-metier-contact">Et posons la première pierre de votre projet</p>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 mb-4">
                <div class="txt-metier">

                </div>
            </div>

        </div>
        <div class="row content-contact">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 contact-form">
                <div class="d-flex justify-content-center">
                    @if (session()->has('success'))
                        <h2 class="text-secondary font-weight-bold text-center">
                            Merci de nous avoir contacté, <br> nous vous contacterons dans les 48 heures qui suivent!
                        </h2>
                    @else
                        <h1 class="text-secondary font-weight-bold text-center">
                        </h1>
                    @endif
                </div>
                <div class="d-flex justify-content-center">
                    <img src="{{asset('asset/images/logo1.png')}}" alt="" class="mx-auto" width="100px" height="auto">
                </div>
                @if (session()->has('success'))

                @else
                    <form action="{{ url('contacter') }}" method="POST" class="row g-3" id="form500">
                        @csrf
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group" style="margin-bottom: 36px">
                                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Nom & Prénom(s)" name="name" value="{{old('name')}}">
                                <span class="@error('name') text-danger @enderror">@error('name'){{ $message }}@enderror</span>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" placeholder="Numéro de téléphone" name="phone" value="{{old('phone')}}">
                                <span class="@error('phone') text-danger @enderror">@error('phone'){{ $message }}@enderror</span>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group" style="margin-bottom: 36px">
                                <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Adresse  Email" name="email" value="{{old('email')}}">
                                <span class="@error('email') text-danger @enderror">@error('email'){{ $message }}@enderror</span>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <input type="text" class="form-control @error('country_city') is-invalid @enderror" id="country_city" placeholder="Pays/Ville" name="country_city" value="{{old('country_city')}}">
                                <span class="@error('country_city') text-danger @enderror">@error('country_city'){{ $message }}@enderror</span>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group" style="">
                                <input type="text" required class="form-control @error('subject') is-invalid @enderror" name="subject" placeholder="Objet" value="{{old('subject')}}">
                                <span class="@error('subject') text-danger @enderror">@error('subject'){{ $message }}@enderror</span>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group" style="">
                                <textarea type="text" required class="form-control @error('country_city') is-invalid @enderror" id="" placeholder="Ecrivez votre message" name="message">{{old('message')}}</textarea>
                                <span class="@error('message') text-danger @enderror">@error('message'){{ $message }}@enderror</span>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 div-btn-submit" style="margin-top: 16px;">
                            <button type="submit" onclick="" class="btn-submit" value="Envoyer">Envoyer</button>
                        </div>
                    </form>
                @endif
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 img-contact" >
                <img src="{{ asset('assets/images/img-contact.png') }}"  class="contact-form-img">
                <div class="card-tel-contact">
                    <div class="text-center-xy">
                        <img src="{{ asset('assets/images/phone-white.png') }}">
                        <p class="telmail">Tel : (225) 79 96 17 23
                            E-mail : info@contractor.ci
                        </p>

                    </div>

                </div>
                <div class="card-location-contact">
                    <div class="text-center-xy">
                        <img src="{{ asset('assets/images/marker-blue.png') }}">
                        <p class="telmail">Boulevard des Armées Zone 3, Treichville, Abidjan</p>
                        <p style="margin-top: 0">Nous sommes ouvert du lundi au
                            vendredi de 9h à 17h
                        </p>
                    </div>
                </div>
            </div>
        </div>
        {{--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.766747824213!2d-3.998615835303173!3d5.299050796155772!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc1eb1d400bbcc3%3A0x3ac8ea86b37f1487!2sSIPPI%20PAPIERS%2C%20SIPPI%20INVESTISSEMENT!5e0!3m2!1sfr!2sci!4v1609770548309!5m2!1sfr!2sci"
                width="100%" height="300px" frameborder="0" style="border:0;margin: 38px 60px auto 60px;" allowfullscreen="" aria-hidden="false" tabindex="0">
        </iframe>--}}
    </div>

@endsection

@section('scripts')

    <script>
         //
        var $input = {
            first_name: $("#first_name"),
            last_name: $("#last_name"),
            email: $("#email"),
            phone_number: $("#phone_number"),
            whatsapp_number: $("#whatsapp_number"),
        }

        $input.phone_number.on('keyup', function(){

            // if($("#dial_code").val() == ''){

            $input.whatsapp_number.val($(this).val())
            // }else{
            //     $input.whatsapp_number.val($("#dial_code").val()+$(this).val())
            // }
        });


        function validateForm(){
            if($input.first_name == null){
                alert('');
            }
            if($input.first_name == null){
                alert('');
            }
            if($input.first_name == null){
                alert('');
            }
            if($input.first_name == null){
                alert('');
            }
        }
        //

    </script>
    <script>
        var input = document.querySelector("#phone_number");

        var errorMsg = document.querySelector("#error-msg"),
            validMsg = document.querySelector("#valid-msg"),
                form500 = document.querySelector("#form500");



        var errorMap = ["&#65794; Numéro Invalide !", "&#65794; Code de pays invalide !", "&#65794; Numéro de téléphone Trop Court", "&#65794; Numéro de téléphone Trop Long", "&#65794; Numéro Invalide"];
        var iti = window.intlTelInput(input, {
            dropdownContainer: document.body,
            separateDialCode: true,
            initialCountry: "CI",
            geoIpLookup: function(success, failure) {
                $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                success(countryCode);
                });
            },
            placeholderNumberType: "MOBILE",
            preferredCountries: ['ci'],
            utilsScript: "{{asset('js/utils.js?')}}",
        });
        var reset = function() {
            input.classList.remove("error");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };

        // on blur: validate
        input.addEventListener('blur', function() {
            reset();
            if (input.value.trim()) {
                if (iti.isValidNumber()) {
                    $("#dial_code").val(iti.getSelectedCountryData().dialCode);
                    // $("#phone_number").val(iti.);
                    $("#country_code").val(iti.getSelectedCountryData().iso2.toUpperCase());
                    // $()
                    // console
                    if($("#dial_code").val() == ''){
                        $('#whatsapp_number').val("225"+$(this).val())
                    }else{
                        $('#whatsapp_number').val($("#dial_code").val()+$("#phone_number").val())
                    }
                    form500.submit();

                    // $("#whatsapp_number").val($("#dial_code").value+$("#phone_number").value);
                    //console.log(iti.s.phone);
                    validMsg.classList.remove("hide");
                } else {
                    input.classList.add("error");
                    var errorCode = iti.getValidationError();
                    // console.log("Error from intInput");
                    // console.log(errorCode);
                    errorMsg.innerHTML = errorMap[errorCode];
                    errorMsg.classList.remove("hide");
                }
            }
        });

        // on keyup / change flag: reset
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);
    </script>

@endsection
