@extends('Customer_FrontOffice.LayoutFrontOffice.masterPage')

@section('title')
    Nos Réalisations | {{ config('app.name') }}
@endsection

@section('content')

    <header class="header-front">
        <div class="row mr-0" id="row-banner">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4 main-breadcrumb" >
                @if( request()->is('projectes') )
                    <p class="breadcrumb-title">Nos Réalisations</p>
                    <ul class="breadcrumb-ul">
                        <li class="breadcrumb-li">
                            <a  href="{{url('/')}}">accueil</a>
                        </li>
                        <div class="vertical-breadcrumb"></div>
                        <li class="breadcrumb-li breadcrumb-li-here" >
                            <a href="#" class="here">Réalisation</a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
    </header>
    <div class="container">
        @foreach ($projects as $projecte)
            <div class="row first-img-realisation" style="">
                <a class="first-img-realisation-link" href="/show_project/{{$projecte->slug}}">
                    <figure class="imghvr-slide-up" id="zoom" style="background-color: inherit;">
                        <img class="img-plus-realisation" src="{{ asset('assets/images/plus.png') }}" >
                        <img src="{{ asset('/storage/images/' .$projecte->image) }}" class="img-real1" style="">
                        <figcaption style="opacity: 0.56" >

                        </figcaption>
                    </figure>
                </a>
                <div class="container-text-first-img-realisation">
                    <h3 class="title-first-img-realisation">{{ $projecte->title }}</h3>
                    @if (isset($projecte->project_owner) && isset($projecte->project_manager) )
                        <p><strong>Maître d’ouvrage :</strong> {{ $projecte->project_owner }}</p>
                        <p><strong>Maître d’oeuvre :</strong>  {{ $projecte->project_manager }}</p>
                        <p><strong>Début des travaux :</strong> {{ \Carbon\Carbon::parse($projecte->start_of_work)->isoFormat('MMMM YYYY') ?? null }}</p>
                        <p><strong>Achèvement des travaux :</strong> {{ \Carbon\Carbon::parse($projecte->end_of_work)->isoFormat('MMMM YYYY') ?? null }}</p>

                        @elseif ($projecte->project_owner == null && isset($projecte->project_manager))
                            <p><strong>Maître d’oeuvre :</strong>  {{ $projecte->project_manager }}</p>
                            <p><strong>Début des travaux :</strong> {{ \Carbon\Carbon::parse($projecte->start_of_work)->isoFormat('MMMM YYYY') ?? null }}</p>
                            <p><strong>Achèvement des travaux :</strong> {{ \Carbon\Carbon::parse($projecte->end_of_work)->isoFormat('MMMM YYYY') ?? null }}</p>

                            @elseif (isset($projecte->project_owner) && $projecte->project_manager == null)
                                <p><strong>Maître d’ouvrage :</strong> {{ $projecte->project_owner }}</p>
                                <p><strong>Début des travaux :</strong> {{ \Carbon\Carbon::parse($projecte->start_of_work)->isoFormat('MMMM YYYY') ?? null }}</p>
                                <p><strong>Achèvement des travaux :</strong> {{ \Carbon\Carbon::parse($projecte->end_of_work)->isoFormat('MMMM YYYY') ?? null }}</p>
                    @endif

                </div>
            </div>
            {{-- @if ($project = 'second')
                <div class="row first-img-realisation" id="first-img-realisation-pc">
                    <div class="container-text-first-img-realisation">
                        <h3 class="title-first-img-realisation" >{{ $projecte->title }}</h3>
                        <p><strong>Maître d’ouvrage :</strong> {{ $projecte->project_owner }}</p>
                        <p><strong>Maître d’oeuvre :</strong>  {{ $projecte->project_manager }}</p>
                        <p><strong>Début des travaux :</strong> {{ $projecte->start_of_work }}</p>
                        <p><strong>Achèvement des travaux :</strong> </p>
                    </div>
                    <a class="first-img-realisation-link" href="{{route('showproject')}}">
                        <figure class="imghvr-slide-up" id="zoom" style="background-color: inherit;">
                            <img class="img-plus-realisation" src="{{ asset('assets/images/plus.png') }}" >
                            <img src="{{ asset('/storage/images/' .$projecte->image) }}" class="img-real1" style="">
                            <figcaption style="opacity: 0.56" >

                            </figcaption>
                        </figure>
                    </a>
                </div>
            @endif --}}
        @endforeach
    </div>
@endsection
