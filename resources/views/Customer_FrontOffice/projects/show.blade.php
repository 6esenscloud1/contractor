@extends('Customer_FrontOffice.LayoutFrontOffice.masterPage')

@section('title')
    Détails Réalisation | {{ config('app.name') }}
@endsection

@section('content')

    <style>
        .project {
            margin: 15px 0;
        }

        .no-gutter .project {
            margin: 0 !important;
            padding: 0 !important;
        }

        .has-spacer {
            margin-left: 30px;
            margin-right: 30px;
            margin-bottom: 30px;
        }

        .has-spacer-extra-space {
            margin-left: 30px;
            margin-right: 30px;
            margin-bottom: 30px;
        }

        .has-side-spacer {
            margin-left: 30px;
            margin-right: 30px;
        }

        .project-title {
            font-size: 1.25rem;
        }

        .project-skill {
            font-size: 0.9rem;
            font-weight: 400;
            letter-spacing: 0.06rem;
        }

        .project-info-box {
            margin: 15px 0;
            background-color: #fff;
            padding: 40px 40px;
            border-radius: 5px solid;
        }

        .project-info-box p {
            margin-bottom: 15px;
            padding-bottom: 15px;
            border-bottom: 1px solid #d5dadb;
        }

        .project-info-box p:last-child {
            margin-bottom: 0;
            padding-bottom: 0;
            border-bottom: none;
        }

        .roundeding img {
            width: 100%;
            max-width: 100%;
            height: auto;
            -webkit-backface-visibility: hidden;
        }

        .rounded {
            border-radius: 5px !important;
        }

        .btn-xs.btn-icon {
            width: 34px;
            height: 34px;
            max-width: 34px !important;
            max-height: 34px !important;
            font-size: 10px;
            line-height: 34px;
        }

        /* facebook button */
        .btn-facebook,
        .btn-facebook:active,
        .btn-facebook:focus {
            color: #fff !important;
            background: #4e68a1;
            border: 2px solid #4e68a1;
        }

        .btn-facebook:hover {
            color: #fff !important;
            background: #3b4f7a;
            border: 2px solid #3b4f7a;
        }

        .btn-facebook-link,
        .btn-facebook-link:active,
        .btn-facebook-link:focus {
            color: #4e68a1 !important;
            background: transparent;
            border: none;
        }

        .btn-facebook-link:hover {
            color: #3b4f7a !important;
        }

        .btn-outline-facebook,
        .btn-outline-facebook:active,
        .btn-outline-facebook:focus {
            color: #4e68a1 !important;
            background: transparent;
            border: 2px solid #4e68a1;
        }

        .btn-outline-facebook:hover {
            color: #fff !important;
            background: #4e68a1;
            border: 2px solid #4e68a1;
        }

        /* twitter button */
        .btn-twitter,
        .btn-twitter:active,
        .btn-twitter:focus {
            color: #fff !important;
            background: #65b5f2;
            border: 2px solid #65b5f2;
        }

        .btn-twitter:hover {
            color: #fff !important;
            background: #5294c6;
            border: 2px solid #5294c6;
        }

        .btn-twitter:hover {
            color: #fff !important;
            background: #5294c6;
            border: 2px solid #5294c6;
        }

        .btn-twitter-link,
        .btn-twitter-link:active,
        .btn-twitter-link:focus {
            color: #65b5f2 !important;
            background: transparent;
            border: none;
        }

        .btn-twitter-link:hover {
            color: #5294c6 !important;
        }

        .btn-outline-twitter,
        .btn-outline-twitter:active,
        .btn-outline-twitter:focus {
            color: #65b5f2 !important;
            background: transparent;
            border: 2px solid #65b5f2;
        }

        .btn-outline-twitter:hover {
            color: #fff !important;
            background: #65b5f2;
            border: 2px solid #65b5f2;
        }

        /* google button */
        .btn-google,
        .btn-google:active,
        .btn-google:focus {
            color: #fff !important;
            background: #e05d4b;
            border: 2px solid #e05d4b;
        }

        .btn-google:hover {
            color: #fff !important;
            background: #b94c3d;
            border: 2px solid #b94c3d;
        }

        .btn-google-link,
        .btn-google-link:active,
        .btn-google-link:focus {
            color: #e05d4b !important;
            background: transparent;
            border: none;
        }

        .btn-google-link:hover {
            color: #b94c3d !important;
        }

        .btn-outline-google,
        .btn-outline-google:active,
        .btn-outline-google:focus {
            color: #e05d4b !important;
            background: transparent;
            border: 2px solid #e05d4b;
        }

        .btn-outline-google:hover {
            color: #fff !important;
            background: #e05d4b;
            border: 2px solid #e05d4b;
        }

        /* linkedin button */
        .btn-linkedin,
        .btn-linkedin:active,
        .btn-linkedin:focus {
            color: #fff !important;
            background: #2083bc;
            border: 2px solid #2083bc;
        }

        .btn-linkedin:hover {
            color: #fff !important;
            background: #186592;
            border: 2px solid #186592;
        }

        .btn-linkedin-link,
        .btn-linkedin-link:active,
        .btn-linkedin-link:focus {
            color: #2083bc !important;
            background: transparent;
            border: none;
        }

        .btn-linkedin-link:hover {
            color: #186592 !important;
        }

        .btn-outline-linkedin,
        .btn-outline-linkedin:active,
        .btn-outline-linkedin:focus {
            color: #2083bc !important;
            background: transparent;
            border: 2px solid #2083bc;
        }

        .btn-outline-linkedin:hover {
            color: #fff !important;
            background: #2083bc;
            border: 2px solid #2083bc;
        }

        /* pinterest button */
        .btn-pinterest,
        .btn-pinterest:active,
        .btn-pinterest:focus {
            color: #fff !important;
            background: #d2373b;
            border: 2px solid #d2373b;
        }

        .btn-pinterest:hover {
            color: #fff !important;
            background: #ad2c2f;
            border: 2px solid #ad2c2f;
        }

        .btn-pinterest-link,
        .btn-pinterest-link:active,
        .btn-pinterest-link:focus {
            color: #d2373b !important;
            background: transparent;
            border: none;
        }

        .btn-pinterest-link:hover {
            color: #ad2c2f !important;
        }

        .btn-outline-pinterest,
        .btn-outline-pinterest:active,
        .btn-outline-pinterest:focus {
            color: #d2373b !important;
            background: transparent;
            border: 2px solid #d2373b;
        }

        .btn-outline-pinterest:hover {
            color: #fff !important;
            background: #d2373b;
            border: 2px solid #d2373b;
        }

        /* dribble button */
        .btn-dribbble,
        .btn-dribbble:active,
        .btn-dribbble:focus {
            color: #fff !important;
            background: #ec5f94;
            border: 2px solid #ec5f94;
        }

        .btn-dribbble:hover {
            color: #fff !important;
            background: #b4446e;
            border: 2px solid #b4446e;
        }

        .btn-dribbble-link,
        .btn-dribbble-link:active,
        .btn-dribbble-link:focus {
            color: #ec5f94 !important;
            background: transparent;
            border: none;
        }

        .btn-dribbble-link:hover {
            color: #b4446e !important;
        }

        .btn-outline-dribbble,
        .btn-outline-dribbble:active,
        .btn-outline-dribbble:focus {
            color: #ec5f94 !important;
            background: transparent;
            border: 2px solid #ec5f94;
        }

        .btn-outline-dribbble:hover {
            color: #fff !important;
            background: #ec5f94;
            border: 2px solid #ec5f94;
        }

        /* instagram button */
        .btn-instagram,
        .btn-instagram:active,
        .btn-instagram:focus {
            color: #fff !important;
            background: #4c5fd7;
            border: 2px solid #4c5fd7;
        }

        .btn-instagram:hover {
            color: #fff !important;
            background: #4252ba;
            border: 2px solid #4252ba;
        }

        .btn-instagram-link,
        .btn-instagram-link:active,
        .btn-instagram-link:focus {
            color: #4c5fd7 !important;
            background: transparent;
            border: none;
        }

        .btn-instagram-link:hover {
            color: #4252ba !important;
        }

        .btn-outline-instagram,
        .btn-outline-instagram:active,
        .btn-outline-instagram:focus {
            color: #4c5fd7 !important;
            background: transparent;
            border: 2px solid #4c5fd7;
        }

        .btn-outline-instagram:hover {
            color: #fff !important;
            background: #4c5fd7;
            border: 2px solid #4c5fd7;
        }

        /* youtube button */
        .btn-youtube,
        .btn-youtube:active,
        .btn-youtube:focus {
            color: #fff !important;
            background: #e52d27;
            border: 2px solid #e52d27;
        }

        .btn-youtube:hover {
            color: #fff !important;
            background: #b31217;
            border: 2px solid #b31217;
        }

        .btn-youtube-link,
        .btn-youtube-link:active,
        .btn-youtube-link:focus {
            color: #e52d27 !important;
            background: transparent;
            border: none;
        }

        .btn-youtube-link:hover {
            color: #b31217 !important;
        }

        .btn-outline-youtube,
        .btn-outline-youtube:active,
        .btn-outline-youtube:focus {
            color: #e52d27 !important;
            background: transparent;
            border: 2px solid #e52d27;
        }

        .btn-outline-youtube:hover {
            color: #fff !important;
            background: #e52d27;
            border: 2px solid #e52d27;
        }

        .btn-xs.btn-icon span,
        .btn-xs.btn-icon i {
            line-height: 34px;
        }

        .btn-icon.btn-circle span,
        .btn-icon.btn-circle i {
            margin-top: -1px;
            margin-right: -1px;
        }

        .btn-icon i {
            margin-top: -1px;
        }

        .btn-icon span,
        .btn-icon i {
            display: block;
            line-height: 50px;
        }

        a.btn,
        a.btn-social {
            display: inline-block;
        }

        .mr-5 {
            margin-right: 5px !important;
        }

        .mb-0 {
            margin-bottom: 0 !important;
        }

        .btn-facebook,
        .btn-facebook:active,
        .btn-facebook:focus {
            color: #fff !important;
            background: #4e68a1;
            border: 2px solid #4e68a1;
        }

        .btn-circle {
            border-radius: 50% !important;
        }

        .project-info-box p {
            margin-bottom: 15px;
            padding-bottom: 15px;
            font-family: "Barlow", sans-serif !important;
            font-weight: 300;
            font-size: 1rem;
            color: #686c6d;
            letter-spacing: 0.03rem;
            margin-bottom: 10px;
            border-bottom: 1px solid #d5dadb;
        }

        b,
        strong {
            font-weight: 700 !important;
        }

        /* DEFILEMENT D'IMAGES */

        /* Slider */

        .slick-slide {
            margin: 0px 20px;
        }

        .slick-slide img {
            width: 100%;
        }

        .slick-slider {
            position: relative;
            display: block;
            box-sizing: border-box;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-touch-callout: none;
            -khtml-user-select: none;
            -ms-touch-action: pan-y;
            touch-action: pan-y;
            -webkit-tap-highlight-color: transparent;
        }

        .slick-list {
            position: relative;
            display: block;
            overflow: hidden;
            margin: 0;
            padding: 0;
        }

        .slick-list:focus {
            outline: none;
        }

        .slick-list.dragging {
            cursor: pointer;
            cursor: hand;
        }

        .slick-slider .slick-track,
        .slick-slider .slick-list {
            -webkit-transform: translate3d(0, 0, 0);
            -moz-transform: translate3d(0, 0, 0);
            -ms-transform: translate3d(0, 0, 0);
            -o-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
        }

        .slick-track {
            position: relative;
            top: 0;
            left: 0;
            display: block;
        }

        .slick-track:before,
        .slick-track:after {
            display: table;
            content: '';
        }

        .slick-track:after {
            clear: both;
        }

        .slick-loading .slick-track {
            visibility: hidden;
        }

        .slick-slide {
            display: none;
            float: left;
            height: 100%;
            min-height: 1px;
        }

        [dir='rtl'] .slick-slide {
            float: right;
        }

        .slick-slide img {
            display: block;
        }

        .slick-slide.slick-loading img {
            display: none;
        }

        .slick-slide.dragging img {
            pointer-events: none;
        }

        .slick-initialized .slick-slide {
            display: block;
        }

        .slick-loading .slick-slide {
            visibility: hidden;
        }

        .slick-vertical .slick-slide {
            display: block;
            height: auto;
            border: 1px solid transparent;
        }

        .slick-arrow.slick-hidden {
            display: none;
        }

    </style>

    <header class="header-front">
        <div class="row mr-0" id="row-banner">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4 main-breadcrumb">
                <p class="breadcrumb-title">Réalisations</p>
                <ul class="breadcrumb-ul">
                    <li class="breadcrumb-li">
                        <a href="{{ url('/') }}">accueil</a>
                    </li>
                    <div class="vertical-breadcrumb"></div>
                    <li class="breadcrumb-li">
                        <a href="{{ route('projectes') }}">Réalisation</a>
                    </li>
                    <div class="vertical-breadcrumb"></div>
                    <li class="breadcrumb-li breadcrumb-li-here">
                        <a href="#" class="here">Détails réalisations</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>

    <div class="container" id="detailReal">
        <div class="row mt-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h2 class="title-page" style="line-height: 50px;">
                    <b>{{ $project->title }}
                    </b>
                </h2>
            </div>
        </div>
        <div class="row-detail-actu">
            <img src="{{ asset('/storage/images/' . $project->image) }}" class="row-detail-actu-first">
        </div>
        <div class="container">
            <div class="row py-4">
                <div class="col-md-7">
                    <div class="project-info-box">
                        <h5>DESCRIPTION DU PROJET</h5>
                        <p class="mb-0">{!! $project->description !!}</p>
                    </div><!-- / project-info-box -->
                </div>
                <div class="col-md-5">
                    <div class="project-info-box">

                        @if (isset($project->project_owner) && isset($project->project_manager))

                            <p><b>Maître d’ouvrage : </b> {{ $project->project_owner }}</p>
                            <p><b>Maître d’oeuvre :</b> {{ $project->project_manager }}</p>
                            <p><b>Début des travaux :</b>
                                {{ \Carbon\Carbon::parse($project->start_of_work)->isoFormat('MMMM YYYY') ?? null }}</p>
                            <p><b>Achèvement des travaux :</b>
                                {{ \Carbon\Carbon::parse($project->end_of_work)->isoFormat('MMMM YYYY') ?? null }}</p>

                        @elseif ($project->project_owner == null && isset($project->project_manager))

                            <p><b>Maître d’oeuvre :</b> {{ $project->project_manager }}</p>
                            <p><b>Début des travaux :</b>
                                {{ \Carbon\Carbon::parse($project->start_of_work)->isoFormat('MMMM YYYY') ?? null }}</p>
                            <p><b>Achèvement des travaux :</b>
                                {{ \Carbon\Carbon::parse($project->end_of_work)->isoFormat('MMMM YYYY') ?? null }}</p>

                        @elseif (isset($project->project_owner) && $project->project_manager == null)

                            <p><b>Maître d’ouvrage : </b> {{ $project->project_owner }}</p>
                            <p><b>Début des travaux :</b>
                                {{ \Carbon\Carbon::parse($project->start_of_work)->isoFormat('MMMM YYYY') ?? null }}</p>
                            <p><b>Achèvement des travaux :</b>
                                {{ \Carbon\Carbon::parse($project->end_of_work)->isoFormat('MMMM YYYY') ?? null }}</p>
                        @endif
                    </div><!-- / project-info-box -->
                </div><!-- / column -->
            </div>
        </div>
    </div>
    <div class="container">
        <div class="owl-carousel owl-theme">
            @if ($project->file == null)
                @php
                $files = explode(',',trim(stripslashes($project->medias),"[]"));
                @endphp
                @foreach ($files as $item)
                    <div class="item">
                        @php
                        $item = trim($item, '"');
                        $url = "/storage/$item";
                        $arr = explode('/', $url);
                        $last = end($arr);
                        $download_link = "/download/$last";
                        @endphp
                        <a href="{{ url($download_link) }}">
                            <img src="{{ asset($url) }}" width="50%"/>
                        </a>
                    </div>
                @endforeach
            @else
                Aucune image de disponible
            @endif
        </div>
    </div>
</div>


    <script>
        $(document).ready(function() {
            $('.customer-logos').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 1500,
                arrows: false,
                dots: false,
                pauseOnHover: false,
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4
                    }
                }, {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 3
                    }
                }]
            });
        });

    </script>


@endsection
