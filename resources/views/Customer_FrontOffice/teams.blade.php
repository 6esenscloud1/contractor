@extends('Customer_FrontOffice.LayoutFrontOffice.masterPage')

@section('title')
   Notre Equipe | {{ config('app.name') }}
@endsection

@section('content')

    <header class="header-front">
        <div class="row mr-0" id="row-banner">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4 main-breadcrumb" >
                @if( request()->is('team') )
                    <p class="breadcrumb-title">L'équipe</p>
                    <ul class="breadcrumb-ul">
                        <li class="breadcrumb-li">
                            <a  href="{{url('/')}}">accueil</a>
                        </li>
                        <div class="vertical-breadcrumb"></div>
                        <li class="breadcrumb-li breadcrumb-li-here" >
                            <a href="#" class="here">Equipe</a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
    </header>
    <div class="how container">
        <div class="row row-txt-equipe">
            <div class="col-12 col-sm-12 col-md-12 col-lg-11 col-xl-11 mb-4">
                <div class="title-metier">
                    <h1 style="font-weight: bold ! important">Notre équipe</h1>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-11 col-xl-11 mb-4">
                <div class="txt-metier">
                    <p class="text-metier">Notre société CONTRACTOR C.I est composée d’une équipe professionnelle d’expérience, dynamique et diversifiée, ayant pour objectif commun d’assurer la qualité d’exécution des prestations.
                        Ces différents profils font notre richesse, grâce à l’expertise individuelle que chacun met au service de l’équipe, pour une étroite collaboration.
                        Découvrez ici les personnes qui rendent possible la vision, la conception et la réalisation de vos projets.
                    </p>
                </div>
            </div>

        </div>
        <div class="owl-carousel owl-theme">
        @foreach ($teams as $team)
            <div class="item">
                <div class="col-12 col-sm-12 container-equipe">
                    <img src="{{ asset('/storage/images/' .$team->image) }}" class="image" style="">
                    <div class="social-link" id="social-link">
                        <img src="{{ asset('assets/images/linkedin-equipe.png') }}" class="fb-linkedin ">
                    </div>
                    <div class="profil">
                        <p class="name">{{ $team->name }}</p>
                        <p class="poste">{{ $team->post }}</p>
                    </div>
                </div>
            </div>
        @endforeach

            {{--  <div class="item">
                <div class="col-12 col-sm-12 container-equipe">
                    <img src="{{ asset('assets/images/equipe2.png') }}" class="image" style="">
                    <div class="social-link" id="social-link">
                        <img src="{{ asset('assets/images/linkedin-equipe.png') }}" class="fb-linkedin ">
                    </div>
                    <div class="profil">
                        <p class="name">Paul Lapierre</p>
                        <p class="poste">Directeur Général</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-12 col-sm-12 container-equipe">
                    <img src="{{ asset('assets/images/equipe3.png') }}" class="image" style="">
                    <div class="social-link" id="social-link">
                        <img src="{{ asset('assets/images/linkedin-equipe.png') }}" class="fb-linkedin ">
                    </div>
                    <div class="profil">
                        <p class="name">Jessica Lahoues</p>
                        <p class="poste">Chargé de projet BTP</p>
                    </div>
                </div>
            </div>  --}}

        </div>
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 row-img-gbonhi" >
                <img src="{{ asset('assets/images/equipe4.png') }}">
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 row-img-gbonhi">
                <img src="{{ asset('assets/images/equipe5.png') }}">
            </div>
        </div>
    </div>

@endsection
@section('myscript')

    <script>
        /*$( document ).ready(function() {
            $(".div-social-link").hide();
        });
        $(".container-equipe").hover(function(){

            $(".div-social-link").show();
            $(".div-social-link").css('transform','translateY(-50%)');

        });*/
    </script>


@endsection
