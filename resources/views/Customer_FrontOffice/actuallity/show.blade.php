@extends('Customer_FrontOffice.LayoutFrontOffice.masterPage')

@section('title')
    Déatail actualité | {{ config('app.name') }}
@endsection

@section('content')

    <header class="header-front">
        <div class="row mr-0" id="row-banner">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4 main-breadcrumb" >
                {{-- @if( request()->is('show_actuality') ) --}}
                    <p class="breadcrumb-title">Actualités</p>
                    <ul class="breadcrumb-ul">
                        <li class="breadcrumb-li">
                            <a  href="{{url('/')}}">accueil</a>
                        </li>
                        <div class="vertical-breadcrumb"></div>
                        <li class="breadcrumb-li">
                            <a  href="{{ route('actuality') }}">Nos Actualités</a>
                        </li>
                        <div class="vertical-breadcrumb"></div>
                        <li class="breadcrumb-li breadcrumb-li-here">
                            <a  href="#" class="here">Detail Actualités</a>
                        </li>
                    </ul>
                {{-- @endif --}}
            </div>
        </div>
    </header>
    <div class="container" id="detailActu">
        <div class="row mt-4">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4">
                <h2 class="title-page" style="line-height: 50px;">
                    <b>{{ $actualitie->title }}
                    </b>
                </h2>
            </div>
        </div>
        <div class="row row-detail-actu">
            <img src="{{ asset('/storage/images/' . $actualitie->image) }}" class="row-detail-actu-first">
        </div>
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 mb-5 text-img-detail-actualite">
                <p>{!! $actualitie->description !!}</p>                {{-- </p><br>
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
                    magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores.</p>

                <img class="img2-detail-actualite" src="{{ asset('assets/images/img2-detail-actualite.png') }}">

                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
                    magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                <br>
                <p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
                    sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
                    vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. </p> --}}
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 mb-4 text-img-detail-actualite-2">
                <img src="{{ asset('assets/images/img1-detail-actualite.png') }}" class="row-detail-actu-second">
                <div class="other1-img-detail-realisation" style="margin-left: 218px !important; width: 350px!important;
                     height: 155px!important;background-color: #f4f4f4;padding: 23px 20px 23px 26px;margin-top: 43px;float: right">
                    <label for="search" class="mb-4">Rechercher</label>
                    <input type="text" id="lname" name="lastname" placeholder="Rechercher" style="width:304px;height: 54px; padding-left:18px"><p style="display: inline-block"></p>
                </div>
            </div>
        </div>

           {{-- <div class="row first-img-detail-actualite" style="margin-top: 43px !important;">
                <div class="text-img-detail-actualite">
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                        et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                    </p><br>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores.

                    </p>
                </div>
                <div class="clearfix-actualite">
                    <img src="{{ url('assets/images/img1-detail-actualite.png') }}" style="width: 350px;height: 380px;float: right">
                </div>
            </div>--}}
        {{--<div class="row second-img-detail-actualite" style="position: relative;margin-top: -94px; margin-left: 0;">--}}
            {{--<div class="">--}}
                {{--<img src="{{ url('assets/images/img2-detail-actualite.png') }}" style="width: 548px;height: 330px; margin-top: -26px">--}}
            {{--</div>--}}
            {{--<div class="other1-img-detail-realisation" style="margin-left: 218px !important; width: 350px!important;--}}
            {{--height: 155px!important;background-color: #f4f4f4;padding: 23px 20px 23px 26px;margin-top: 143px">--}}
                {{--<label for="search">Rechercher</label>--}}
                {{--<input type="text" id="lname" name="lastname" placeholder="Rechercher" style="width:304px;height: 54px;"><p style="display: inline-block"></p>--}}
            {{--</div>--}}
        {{--</div>--}}
       {{-- <div class="row six-img-detail-activite" style="margin-top: 43px; margin-left: 0;width: 710px;">
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
            </p><br>
            <p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

        </div>--}}
    </div>

@endsection
