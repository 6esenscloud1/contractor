@extends('Customer_FrontOffice.LayoutFrontOffice.masterPage')

@section('title')
    Notre Actualité | {{ config('app.name') }}
@endsection

@section('content')

     <header class="header-front">
        <div class="row mr-0" id="row-banner">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4 main-breadcrumb" >
                @if( request()->is('actuality') )
                    <p class="breadcrumb-title">Toutes les Actualités</p>
                    <ul class="breadcrumb-ul">
                        <li class="breadcrumb-li">
                            <a  href="{{url('/')}}">accueil</a>
                        </li>
                        <div class="vertical-breadcrumb"></div>
                        <li class="breadcrumb-li breadcrumb-li-here">
                            <a  href="#" class="here">Actualités</a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
    </header>
    <div class="container" id="actualites">

        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="title-metier">
                    <h1 style="font-weight: bold ! important">Les actualités</h1>
                </div>
            </div>
        </div>

        <div class="row row-actualites" id="row-page-actualite" style="margin-top: 35px;">
            @foreach ($actualities as $actualitie)
                <div class="col-sm-4 div-img">
                    <div class="item-actu item-actu-actualite">
                        <a href="/show_actuality/{{$actualitie->slug}}">
                            <div class="first-img-actu">
                                <img src="{{ asset('/storage/images/' .$actualitie->image) }}" alt="{{ $actualitie->title }}">
                            </div>
                            <div class="img-actu-date">
                                <p>{{ \Carbon\Carbon::parse($actualitie->actuality_date)->format('d') ?? null }}</p>
                                <p style="line-height: 3em">{{ \Carbon\Carbon::parse($actualitie->actuality_date)->getTranslatedMonthName('MMMM') ?? null }}</p>
                            </div>
                            <div class="body-item-actu">
                                <p>{{ $actualitie->title ?? '' }}</p>
                                <p class="txt-actu">{{ $actualitie->subtitle ?? '' }}</p>
                                <p class="txt-know"><span class="txt-suite">En savoir plus</span><i class="fa fa-chevron-right" aria-hidden="true" style="color: #28a1a2;display: inline-flex"></i></p>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- FIN ACTUALITES -->
    </div>

@endsection
