<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <link rel="icon" href="{{ asset('assets/images/logo1.png') }}" style="height: 12px">

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <!-- Montserrat Font -->
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">


        <!-- Scripts -->
        <script src="{{ asset('assets/js/app.js') }}" defer></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/styles.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/footer.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/jquery.fancybox.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/Hover-master/css/hover.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/imagehover/css/imagehover.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />


    </head>

    <body id="page-top">

        @include('Customer_FrontOffice.LayoutFrontOffice.header')

            @yield('content')

        @include('Customer_FrontOffice.LayoutFrontOffice.footer')


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
        <script src="{{ asset('assets/js/caroussel.js') }}"></script>
    </body>

    <!-- jQuery -->
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Bootstrap -->
    {{-- <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script> --}}
    <script src="{{ asset('assets/js/jquery.fancybox.min.js') }}"></script>
    <script src="{{ asset('assets/js/cookies.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.jcarousel.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    {{--JAVASCRIPT CODE--}}
    <script>
    /* //Get the button
        var mybutton = document.getElementById("myBtn");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                mybutton.style.display = "block";
            } else {
                mybutton.style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }*/
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("myBtn").style.opacity = "1";
        } else {
            document.getElementById("myBtn").style.opacity = "0";
        }
    }

    function topFunction() {
        $('html,body').animate({ scrollTop: 0 }, 1000);
    }


    // Add active class to the current button (highlight it)
        var header = document.getElementById("navbar-nav");
        var btns = header.getElementsByClassName("nav-link");
        for (var i = 0; i < btns.length; i++) {
            btns[i].addEventListener("click", function() {
                var current = document.getElementsByClassName("active");
                current[0].id = current[0].id.replace(" active", "");
                this.id += "active";
            });
        }
    </script>

    {{--JQUERY CODE--}}
    <script>
        $(document).ready(function() {

            /*$("ul.navbar-nav li a").click(function() {
                $("ul.navbar-nav li a").removeClass("active");
                $(this).addClass("active");
            });*/

            var index = Cookies.get('active');
            index = (typeof index == 'number') ? index : 0;
            $('#navbar-nav').find('a').removeClass('active');
            $("#navbar-nav").find('a').eq(index).addClass('active');
            $('#navbar-nav').on('click', 'li a', function (e) {
                // e.preventDefault();
                e.stopPropagation();
                $('#navbar-nav').find('a').removeClass('active');
                $(this).addClass('active');
                Cookies.set('active', $('#navbar-nav a').index(this));
            });

            $("[data-trigger]").on("click", function(){
                var trigger_id =  $(this).attr('data-trigger');
                $(trigger_id).toggleClass("show");
                $('body').toggleClass("offcanvas-active");
            });

            // close if press ESC button
            $(document).on('keydown', function(event) {
                if(event.keyCode === 27) {
                    $(".navbar-collapse").removeClass("show");
                    $("body").removeClass("overlay-active");
                }
            });

            // close button
            $(".btn-close").click(function(e){
                $(".navbar-collapse").removeClass("show");
                $("body").removeClass("offcanvas-active");
            });

            $(".call-submenu").on("click", function(){
                $("#navbar-toggler-submenu").trigger("click");
            });
            $(".return-submenu").on("click", function(){
                $(".btn-close").trigger("click");
                $(".main-navbar-toggler").trigger("click");
            });



        });
    </script>


    @include('sweetalert::alert')
    @yield('myscript')

</html>
