<div class="sect-logo">
    <img src="{{ asset('assets/images/logo.png') }}">
</div>
<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                    <div class="restons-co">
                        <h3>Restons connectés</h3>
                        <ul>
                            <li class="map">Boulevard des Armées, zone 3,<br/><span class="ss">Treichville, Abidjan</span></li>
                            <li class="m-footer">info@contractor.ci</li>
                            <li class="t-footer"> (+225) 07 79 96 96 96</li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="lien-utiles">
                        <h3>Liens utiles</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="bloc1">
                                    <ul>
                                        <li><a href="{{ url('/') }}">Accueil</a></li>
                                        <li><a href="{{ route ('about') }}">A Propos</a></li>
                                        <li><a href="{{ route ('team') }}">L'équipe</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <ul>
                                    <li><a href="{{ route ('projectes') }}">Nos Réalisations</a></li>
                                    <li><a href="{{ route ('actuality') }}">Actualités</a></li>
                                    <li><a href="{{route ('contact') }}">Contacts</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="joi">
                        <h3>Joindre le support</h3>
                        <p class="num">+225 22 21 25 15 70</p>
                        <p class="txt">Nous sommes disponibles du lundi au vendredi de 08h à 17h.</p>
                    </div>
                </div>
        </div>

    </div>
</div>
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="txt-copy">
                    <p>Copyright © 2020 <span class="dd">Contractor</span> Tous droits réservés.</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="rs-copyright">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true">&nbsp;</i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true">&nbsp;</i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true">&nbsp;</i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

    <a onclick="topFunction()" id="myBtn" class="scroll">
        <i class="fa fa-chevron-up"></i>
    </a>

    <div class="mobile">
        <div class="btn-mobile">
            <div class="row">
                <div class="col-6">
                    <a href="mailto:info@contractor.ci">
                        <div class="btn-message bt-m-msg">
                            MESSAGE
                        </div>
                    </a>

                </div>
                <div class="col-6">
                    <a href="tel:+225 21251570">
                        <div class="btn-call bt-m-call">
                            APPELER
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

