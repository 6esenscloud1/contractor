<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <link rel="icon" href="{{ asset('assets/images/logo1.png') }}" style="height: 12px">

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <!-- Poppins Font -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

        <!-- Montserrat Font -->
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

        <!-- Carroussel CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />

        <!-- Scripts -->
        <script src="{{ asset('assets/js/app.js') }}" defer></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">


        <!-- Styles -->
        <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('assets/css/footer.css') }}">
        <link href="{{ asset('assets/css/styles.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/jquery.fancybox.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/Hover-master/css/hover.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/imagehover/css/imagehover.min.css') }}" rel="stylesheet">
        {{--<link href="{{ asset('assets/css//ruang-admin.min.css') }}" rel="stylesheet">--}}


    </head>

    <body>
        <main class="mt-0">

            @include('Customer_FrontOffice.LayoutFrontOffice.header')

            @yield('content')

            @include('Customer_FrontOffice.LayoutFrontOffice.footer')

        </main>

        <!-- jQuery -->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>

        <!-- Bootstrap -->
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/OwlCarousel2-2.3.4/dist/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.fancybox.min.js') }}"></script>
        <script src="{{ asset('assets/js/cookies.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.jcarousel.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

        <!-- Carrousselle JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
        {{-- <script src="{{ asset('assets/js/caroussel.js') }}"></script> --}}

        {{--JAVASCRIPT CODE--}}
        <script>
            window.onscroll = function() {scrollFunction()};

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById("myBtn").style.opacity = "1";
                } else {
                    document.getElementById("myBtn").style.opacity = "0";
                }
            }

            function topFunction() {
                $('html,body').animate({ scrollTop: 0 }, 1000);
            }


        </script>

        {{--JQUERY CODE--}}
        <script>

            $(document).ready(function() {

                /*$("ul.navbar-nav li a").click(function() {
                    $("ul.navbar-nav li a").removeClass("active");
                    $(this).addClass("active");
                });*/

                var index = Cookies.get('active');
                index = (typeof index == 'number') ? index : 0;
                $('#navbar-nav').find('a').removeClass('active');
                $("#navbar-nav").find('a').eq(index).addClass('active');
                $('#navbar-nav').on('click', 'li a', function (e) {
                    // e.preventDefault();
                    e.stopPropagation();
                    $('#navbar-nav').find('a').removeClass('active');
                    $(this).addClass('active');
                    Cookies.set('active', $('#navbar-nav a').index(this));


                });


                $("[data-trigger]").on("click", function(){
                    var trigger_id =  $(this).attr('data-trigger');
                    $(trigger_id).toggleClass("show");
                    $('body').toggleClass("offcanvas-active");
                });

                // close if press ESC button
                $(document).on('keydown', function(event) {
                    if(event.keyCode === 27) {
                        $(".navbar-collapse").removeClass("show");
                        $("body").removeClass("overlay-active");
                    }
                });

                // close button

                $(".btn-close").click(function(e){
                    $(".navbar-collapse").removeClass("show");
                    $("body").removeClass("offcanvas-active");
                });

                $(".call-submenu").on("click", function(){
                    $("#navbar-toggler-submenu").trigger("click");
                });
                $(".return-submenu").on("click", function(){
                    $(".btn-close").trigger("click");
                    $(".main-navbar-toggler").trigger("click");
                });

            });



            $('.carousel').carousel({
                touch: true
            })

            /*  $(document).on("click","a.scroll-to-top",(function(e){
                var $anchor=$(this);
                $("html, body").stop().animate({
                    scrollTop:$($anchor.attr("href")).offset().top
                },1e3,"easeInOutExpo"),
                    e.preventDefault()
                })
            );
        */
            $('.carousel').on('touchstart', function(event){
                const xClick = event.originalEvent.touches[0].pageX;
                $(this).one('touchmove', function(event){
                    const xMove = event.originalEvent.touches[0].pageX;
                    const sensitivityInPx = 5;

                    if( Math.floor(xClick - xMove) > sensitivityInPx ){
                        $(this).carousel('next');
                    }
                    else if( Math.floor(xClick - xMove) < -sensitivityInPx ){
                        $(this).carousel('prev');
                    }
                });
                $(this).on('touchend', function(){
                    $(this).off('touchmove');
                });
            });

        </script>

        <script>

            $(".more").hover(function(){
                $(this).stop()
                    .animate({
                        width: '225px',
                        left: $(this).find('.txt-suite').width()
                    }, 200)
                    .css({ 'z-index' : '10' });
            }, function () {
                $(this).stop()
                    .animate({
                        width: '17px',
                        left: '213px',
                        height: '17px'
                    }, 200)
                    .css({'z-index' : '1'});
            });

            /*$(function() {
                $(".jcarousel").jcarousel({
                    wrap: 'circular'
                });

                $(".jcarousel").jcarouselAutoscroll({
                    target: '+=1',
                    autostart: true,
                });
            });*/

        </script>




    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/js/caroussel.js') }}"></script>
</body>
</html>
