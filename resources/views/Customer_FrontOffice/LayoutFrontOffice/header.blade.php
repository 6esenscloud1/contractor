<div class="container-fluid" id="h-container">
    <div class="row ml-auto div-top-h">
        <div class="col-5 col-sm-4 col-md-4 col-lg-4 col-xl-4 div-top-h-top-first">
            <p>Votre fournisseur de services 24h/24</p>
            <span class="mobile-sss">Services 24h/24 </span>
        </div>
        <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 div-top-h-top-second">
            <p class="map-hh"><a href="#" style="text-decoration: none;color: inherit">Trouvez-nous sur la carte</a></p>
            <a href="#" style="text-decoration: none;color: inherit" class="map-mobile"><img
                    src="{{ asset('assets/images/marker-blue.png') }}" alt="" srcset=""></a>
        </div>
        <div class="col-3 col-sm-4 col-md-4 col-lg-4 col-xl-4 div-top-h-top-third">
            <p class="contact-h">Contact: 225 07 57 56 55 54</p>
            <a class="fb-ic link-icon" href="#">
                <i class="fa fa-facebook ml-2"> </i>
            </a>
            <!--Twitter-->
            <a class="tw-ic link-icon" href="#">
                <i class="fa fa-twitter ml-2"> </i>
            </a>
            <!--Linkedin-->
            <a class="li-ic link-icon" href="#">
                <i class="fa fa-linkedin ml-2"> </i>
            </a>
        </div>
    </div>
    <div class="row div-bottom-h">
        <div class="col-lg-4 col-xl-4 div-bottom-h-first">
            <a class="nav-logo" data-tab="#home" href="{{ url('/') }}"><img
                    src="{{ asset('assets/images/logo.png') }}"></a>
        </div>
        <div class="col-lg-3 col-xl-3 div-bottom-h-second">
            <div class="text-phone" style="float: right">
                <img src="{{ asset('assets/images/phone.png') }}" class="icon-h" style="float: left">
                <p style="margin-bottom: 0;">Contactez-nous</p>
                <p class="number-phone">+225 07 57 56 55 54</p>
            </div>
        </div>
        <div class="col-lg-3 col-xl-3 div-bottom-h-third">
            <div class="text-phone" style="">
                <img src="{{ asset('assets/images/enveloppe.png') }}" class="icon-h" style="float: left;">
                <p style="margin-bottom: 0;">Envoyez-nous un email</p>
                <p class="number-phone">info@contractor.ci</p>
            </div>
        </div>
        <div class="col-lg-2 col-xl-2 div-bottom-h-fourth">
            <div class="text-phone" style="">
                <img src="{{ asset('assets/images/marker.png') }}" class="icon-h" style="float: left;">
                <p style="margin-bottom: 0;">Retrouvez-nous</p>
                <p class="number-phone marker-number">Abidjan-Cocody</p>
            </div>
        </div>
    </div>


</div>
{{-- position: absolute --}}

<nav class="navbar navbar-expand-lg navbar-light bg-light" id="pc-navbar">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
        aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <div class="row mt-3">
            <ul class="navbar-nav mr-auto" id="navbar-nav">

                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Accueil</a>
                </li>
                <li class="nav-item" id="dropdown">
                    <a class="nav-link" href="#">L'entreprise</a>
                    <ul class="submenu">
                        <li class="submenu-item">
                            <a class="" href="{{ route('about') }}" style="top: 2em !important">A propos</a>
                        </li>

                        <li class="submenu-item">
                            <a class="" href="{{ route('team') }}" style="top: 2em !important">L'équipe</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('projectes') }}">Réalisations</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('actuality') }}">Actualités</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('contact') }}">Contacts</a>
                </li>

            </ul>

        </div>
    </div>
</nav>


<nav class="navbar navbar-expand-lg navbar-dark bg-primary" id="mobile-navbar">

    <button class="navbar-toggler main-navbar-toggler" type="button" data-trigger="#main_nav">
        <span class="navbar-toggler-icon"></span>
    </button>
    <button class="navbar-toggler" type="button" data-trigger="#main_nav_submenu" id="navbar-toggler-submenu">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse" id="main_nav">
        <div class="offcanvas-header mt-3">
            <img src="{{ asset('assets/images/logo.png') }}" class="logo-menu-mobile">
            <button class="btn btn-close float-right"> Fermer </button>
            <br><br><br>
        </div>
        <ul class="navbar-nav">
            <li class="nav-item main-nav-item"> <a class="nav-link" data-tab="#home"
                    href="{{ url('/') }}">Accueil</a> </li>
            <li class="nav-item dropdown main-nav-item">
                <a class="nav-link dropdown-toggle call-submenu "> L'entreprise &nbsp;&nbsp;
                    <i class="fa fa-chevron-right" aria-hidden="true"
                        style="color: #28a1a2;display: inline-flex"></i></a>
            </li>
            <li class="nav-item main-nav-item"> <a class="nav-link " data-tab="#home"
                    href="{{ route('projectes') }}">Réalisations</a> </li>
            <li class="nav-item main-nav-item"> <a class="nav-link " data-tab="#home"
                    href="{{ route('actuality') }}">Actualités</a> </li>
            <li class="nav-item main-nav-item"> <a class="nav-link " data-tab="#home"
                    href="{{ route('contact') }}">Contacts</a> </li>
        </ul>
    </div>
    <!-- navbar-collapse.// -->

    <div class="navbar-collapse" id="main_nav_submenu">
        <div class="offcanvas-header mt-3">
            <img src="{{ asset('assets/images/logo.png') }}" class="logo-menu-mobile">
            <button class="btn btn-close float-right"> Fermer </button>
        </div>
        <ul class="navbar-nav">
            <li class="nav-item dropdown dropdown-mobile">
                <a class="nav-link dropdown-toggle return-submenu" style="color: #002b5d !important;"><i
                        class="fa fa-chevron-left" aria-hidden="true" style="color: #002b5d;display: inline-flex"></i>
                    &nbsp;&nbsp; L'entreprise
                </a>
            </li>
            <li class="nav-item nav-item-submenu"> <a class="nav-link" data-tab="#home" href="{{ route('about') }}">A
                    propos</a> </li>
            <li class="nav-item nav-item-submenu"> <a class="nav-link" data-tab="#home"
                    href="{{ route('team') }}">L'équipe</a> </li>
        </ul>
    </div>
</nav>
