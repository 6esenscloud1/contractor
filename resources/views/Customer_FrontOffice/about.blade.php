@extends('Customer_FrontOffice.LayoutFrontOffice.masterPage')

@section('title')
    A propos de Nous | {{ config('app.name') }}
@endsection

@section('content')

    <header class="header-front">
        <div class="row mr-0" id="row-banner">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4 main-breadcrumb" >
                @if( request()->is('about') )
                    <p class="breadcrumb-title">A propos</p>
                    <ul class="breadcrumb-ul">
                        <li class="breadcrumb-li">
                            <a  href="{{url('/')}}">accueil</a>
                        </li>
                        <div class="vertical-breadcrumb"></div>
                        <li class="breadcrumb-li breadcrumb-li-here" >
                            <a  href="#" class="here">A propos</a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
    </header>
    <div class="container">

        <div class="row first-img-realisation-propos" style="">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <img src="{{ asset('assets/images/propos.png') }}" >
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <p class="title-first-img-realisation">A propos de Contractor</p>
                <p class="txt-first-img-realisation">CONTRACTOR CI est une société anonyme au capital de 200 000 000 FCFA, spécialisée dans la construction. Basée en Côte d’Ivoire depuis 2015, nous réalisons des projets dans les domaines du génie civil, de la construction. Notre expertise, nous la mettons aussi bien au service des acteurs du secteur public qu’à ceux du privé.
                    En tant que bâtisseurs d’infrastructures durables, notre ambition est de contribuer au développement d’une Afrique en pleine émergence.<br><br>CONTRACTOR CI, c’est : </p>
                <ul class="ul-propos">
                    <li><p style="color:  #08A1A3;margin-bottom: 0;margin-top: 25px;">Un objectif :</p></li>
                    <p class="txt-first-img-realisation">Concevoir vos projets et employer des stratégies logistiques et humaines les plus adaptées à vos besoins, en harmonie avec notre environnement.</p>
                    <li><p style="color:  #08A1A3;margin-bottom: 0;">Un engagement :</p></li>
                    <p class="txt-first-img-realisation">Mettre à votre disposition notre expertise technique et notre savoir faire.</p>
                    <li><p style="color:  #08A1A3;margin-bottom: 0;">Une force :</p></li>
                    <p class="txt-first-img-realisation">Des équipements modernes à la pointe de la technologie, des partenariats solides avec l’international et enfin et surtout,
                        un personnel expérimenté et local ayant une parfaite maîtrise du marché ivoirien.</p>
                </ul>
            </div>
        </div>


    </div>

@endsection
