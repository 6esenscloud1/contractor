@extends('Customer_FrontOffice.LayoutFrontOffice.master')

@section('title')
    Accueil | {{ config('app.name') }}
@endsection

@section('content')

    <!-- METIERS -->
    <header class="header-banner">

        <div class="container-fluid slide-home">
            <div class="row">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner carou">
                        @foreach ($slides as $slide)
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="{{ asset('/storage/images/' . $slide->picture1) }}"
                                    alt="First slide">
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="row mr-0" id="row-banner">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-4 text-banner">
                La perfection se <p class="greenText" style="display: inline-flex">construit</p> tous les jours
            </div>
        </div>

    </header>

    <div class="how container" id="metiers">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4">
                <div class="title-metier">
                    <h1>Nos métiers</h1>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-4">
                <div class="txt-metier">
                    <p class="text-metier">Contractor CI intervient sur l’ensemble des secteurs assetss et privés, en neuf
                        comme en réhabilitation.
                        La capacité d’innovation de Contractor CI lui permet d’obtenir de nouveaux marchés : Son expertise
                        dans la construction de bâtiments
                        et les solutions modulaires préfabriqués assure une réalisation méticuleuse et qualitative de vos
                        projets.
                    </p>
                </div>
            </div>
        </div>
        <div class="owl-carousel owl-theme">
            @foreach ($trades as $trade)
                <div class="item">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 mb-4 div-img" id="modal0" data-toggle="modal"
                        data-target="#genie-civil-Modal{{ $trade->id }}" style="">
                        <img src="{{ asset('/storage/images/' . $trade->image) }}" alt="" class="" />
                        <img src="{{ asset('assets/images/plus.png') }}" alt="" class="plus-md" />
                        <div class="txt-img-metier">
                            <p>{{ $trade->title }}</p>
                            <p class="txt-">{{ $trade->subtitle }}</p>
                        </div>
                        <div class="overlay"></div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    {{-- modal travaux publics --}}
    @foreach ($trades as $trade)
        <div class="modal fade" id="genie-civil-Modal{{ $trade->id }}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-body-image">
                        <img src="{{ asset('/storage/images/' . $trade->image) }}" style="width: 652px; height: 356px" />
                    </div>
                    <div class="modal-body-text">
                        {!! $trade->description !!}
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <!-- FIN METIERS -->

    <!-- RSE -->
    <div class="how" id="rse">
        <div class="div-rse" style="margin-top: 105px">
            <div class="rse-container">
                @foreach ($category_blocs as $category_bloc)
                    <div class="txt-rse">
                        <p class="">{{ $category_bloc->title }}</p>
                        <p class="dt-rse">{!! $category_bloc->description !!} </p>
                    </div>
                @endforeach

                <div class="owl-carousel owl-theme caro">
                    @foreach ($bloc_fixs as $bloc_fix)
                        <div class="item">
                            <div>
                                <div class="box-rse">
                                    <div class="txt-box-rse">
                                        <p class="title-box-rse">{{ $bloc_fix->title }}</p>
                                        <p>{!! $bloc_fix->description !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <!--FIN RSE -->
    <!-- ACTUALITES -->
    <div class="how container" id="actualites">
        <div class="mx-auto mb-md-0 mb-4 text-center" id="div-title-actu">
            <div class="title-actu text-center">
                <h1>Actualités</h1>
            </div>
        </div>
        <div class="owl-carousel owl-theme">
            @foreach ($actualities as $actualite)
                <div class="item">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 mb-4 div-img">
                        <div class="item-actu">
                            <a href="/show_actuality/{{ $actualite->slug }}">
                                <div class="first-img-actu">
                                    <img src="{{ asset('/storage/images/' . $actualite->image) }}">
                                </div>
                                <div class="img-actu-date">
                                    <p>{{ \Carbon\Carbon::parse($actualite->actuality_date)->format('d') ?? null }}</p>
                                    <p style="line-height: 2em">
                                        {{ \Carbon\Carbon::parse($actualite->actuality_date)->getTranslatedMonthName('MMMM') ?? null }}
                                    </p>
                                </div>
                                <div class="body-item-actu">
                                    <p>{{ $actualite->title }}</p>
                                    <p class="txt-actu">{{ $actualite->subtitle }}</p>
                                    <p class="txt-know"><span class="txt-suite">En savoir plus</span></p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <!-- FIN ACTUALITES -->

    <!--FIN ACTUALITES -->
    </div>


@endsection
