<div class="row mt-3">
    <div class="col-md-6">
        <!-- title At Field -->
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            <p>{{ $actuality->title }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- subtitle At Field -->
        <div class="form-group">
            {!! Form::label('subtitle', 'Sous-Titre:') !!}
            <p>{{ $actuality->subtitle }}</p>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            <p>{!! $actuality->description !!}</p>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-6">
        <!-- subtitle At Field -->
        <div class="form-group">
            {!! Form::label('actuality_date', 'Date du Poste:') !!}
            <p>{{ $actuality->actuality_date }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- subtitle At Field -->
        <div class="form-group">
            {!! Form::label('image', 'Image de garde:') !!}
            <p><img src="{{ asset('/storage/images/' . $actuality->image) }}" alt="{{ $actuality->title }}" width="250" height="250"></p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Date de création:') !!}
            <p>{{ $actuality->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Updated At Field -->
        <div class="form-group">
            {!! Form::label('updated_at', 'Date de modification:') !!}
            <p>{{ $actuality->updated_at }}</p>
        </div>
    </div>
</div>




