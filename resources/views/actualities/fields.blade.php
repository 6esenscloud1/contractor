<div class="row mt-3">
    <div class="col-md-4">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            {!! Form::text('title', null, ['class' => 'form-control', 'onkeyup' => 'stringToSlug(this.value)']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control', 'readonly']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <!-- Subtitle Field -->
        <div class="form-group">
            {!! Form::label('subtitle', 'Sous-Titre:') !!}
            {!! Form::text('subtitle', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>


<div class="row mt-3">
    <!-- description Field -->
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'name' => 'description', 'id' => 'description']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <!-- actuality_date Field -->
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('actuality_date', 'Date du Poste:') !!}
            @if(isset($actuality))
                {!! Form::date('actuality_date', \Carbon\Carbon::parse($actuality->actuality_date)->format('d-m-Y') ?? null, ['class' => 'form-control', 'id' => 'actuality_date','value' => 0]) !!}
            @else
                {!! Form::date('actuality_date', null, ['class' => 'form-control', 'id' => 'actuality_date','value' => 0]) !!}
            @endif
        </div>
    </div>
    <!-- image Field -->
    <div class="col-md-6">
        <div class="form-group">
            <strong>{!! Form::label('image', 'Image de Garde:') !!}</strong>
            {!! Form::file('image', ['class' => 'form-control', 'onchange' => 'loadFile(event)']) !!}
        </div>
    </div>
</div>
@if ($actuality ?? '')
    <div class="row">
        <div class="col-md-6 output">
            <div class="form-group">
                <img src="{{ asset('/storage/images/' . $actuality->image) }}" alt="{{ $actuality->title }}" width="250" height="250" />
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class="form-group">
                <img style="width: 50%; height: 50%; border: none" id="output" />
            </div>
        </div>
    </div>
@endif
<div class="row mt-3">
    {{--<div class="col-md-6">
        <div class="form-group">
            <input type="checkbox" name="is_free" value="0">
        </div>
    </div>--}}
</div>

<script>
    var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById('output');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };

    function showPrice(val) {
        if (val == 'NON') {
            document.getElementById("entry_free").style.display = 'block';
        } else {
            document.getElementById("entry_free").style.display = 'none';
        }
    }

</script>


</script>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('actualities.index') }}" class="btn btn-secondary">Annulé</a>
</div>


<script>
    function stringToSlug(str) {

        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        // return str;
        document.getElementById("slug").value = str;
    }

    function showPercentage(val){
        if(val == 1){
            document.getElementById("div_percentage").style.display= 'block';
        }else{
            document.getElementById("div_percentage").style.display= 'none';
        }
    }

    function showSold(val){
        if(val == 1){
            document.getElementById("div_sold").style.display= 'block';
        }else{
            document.getElementById("div_sold").style.display= 'none';
        }
    }
</script>
