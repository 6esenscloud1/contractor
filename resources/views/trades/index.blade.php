{{--  @extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Trades</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Trades
                             <a class="pull-right" href="{{ route('trades.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                         </div>
                         <div class="card-body">
                             @include('trades.table')
                              <div class="pull-right mr-3">

                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection
  --}}

@extends('Admin_BackOffice.Admin_layouts.master')

@section('title')
    Nos métiers | {{ config('app.name') }}
@endsection

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title mt-0"> Nos métiers</h4>
                            <p class="card-category"> Tous nos métiers</p>
                            <div class="col text-right">
                                <a class="pull-right btn btn-sm btn-light" href="{{ route('trades.create') }}">Ajouter un
                                métier</a>
                            </div>
                        </div>
                        <div class="card-body">
                            @include('trades.table')
                        </div>
                        <div class="mt-3 d-flex justify-content-center">
                        {{--  {{ $trades->links() }}  --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

