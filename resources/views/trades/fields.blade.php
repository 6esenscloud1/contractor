<div class="row mt-3">
    <div class="col-md-4">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            {!! Form::text('title', null, ['class' => 'form-control', 'onkeyup' => 'stringToSlug(this.value)']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <!-- Slug Field -->
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control', 'readonly']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <!-- Subtitle Field -->
        <div class="form-group">
            {!! Form::label('subtitle', 'Sous-Titre:') !!}
            {!! Form::text('subtitle', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- description Field -->
<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::textarea('description', null, ['class' => 'ckeditor form-control', 'name' => 'description']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            <strong>{!! Form::label('image', 'Image:') !!}</strong>
            {!! Form::file('image', ['class' => 'form-control', 'onchange' => 'loadFile(event)']) !!}
        </div>
    </div>
</div>
@if ($trade ?? '')
    <div class="row">
        <div class="col-md-6 output">
            <div class="form-group">
                <img src="{{ asset('/storage/images/' . $trade->image) }}" alt="{{ $trade->title }}" width="250" height="250">
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class="form-group">
                <img style="width: 50%; height: 50%; border: none" id="output" />
            </div>
        </div>
    </div>
@endif
<div class="row mt-3">
    {{--<div class="col-md-6">
        <div class="form-group">
            <input type="checkbox" name="is_free" value="0">
        </div>
    </div>--}}
</div>

<script>
    var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById('output');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };

    function showPrice(val) {
        if (val == 'NON') {
            document.getElementById("entry_free").style.display = 'block';
        } else {
            document.getElementById("entry_free").style.display = 'none';
        }
    }

</script>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('trades.index') }}" class="btn btn-secondary">Annulé</a>
</div>



<script>
    function stringToSlug(str) {

        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        // return str;
        document.getElementById("slug").value = str;
    }

    function showPercentage(val){
        if(val == 1){
            document.getElementById("div_percentage").style.display= 'block';
        }else{
            document.getElementById("div_percentage").style.display= 'none';
        }
    }

    function showSold(val){
        if(val == 1){
            document.getElementById("div_sold").style.display= 'block';
        }else{
            document.getElementById("div_sold").style.display= 'none';
        }
    }
</script>

