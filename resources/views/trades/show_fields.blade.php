<div class="row mt-3">
    <div class="col-md-6">
        <!-- title At Field -->
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            <p>{{ $trade->title }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- subtitle At Field -->
        <div class="form-group">
            {!! Form::label('subtitle', 'Sous-Titre:') !!}
            <p>{{ $trade->subtitle }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            <p>{!! $trade->description !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('image', 'Image:') !!}
            <p><img src="{{ asset('/storage/images/' . $trade->image) }}" alt="{{ $trade->title }}"
                width="250" height="250"></p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Date de création:') !!}
            <p>{{ $trade->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Updated At Field -->
        <div class="form-group">
            {!! Form::label('updated_at', 'Date de modification:') !!}
            <p>{{ $trade->updated_at }}</p>
        </div>
    </div>
</div>



