{{-- @extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('trades.index') !!}">Trade</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Trade</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($trade, ['route' => ['trades.update', $trade->id], 'method' => 'patch']) !!}

                              @include('trades.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection --}}

@extends('Admin_BackOffice.Admin_layouts.master')

@section('title')
    Nos métiers | {{ config('app.name') }}
@endsection

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Modifier un métier</h4>
                            <p class="card-category">Information sur un métier</p>
                        </div>
                        <div class="card-body">
                            {!! Form::model($trade, ['route' => ['trades.update', $trade->id], 'method' => 'patch', 'files' => true]) !!}

                              @include('trades.fields')

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
