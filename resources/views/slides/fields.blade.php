<div class="row mt-3">
    <!-- Picture1 Field -->
    <div class="col-md-6">
        <div class="form-group">
            <strong>{!! Form::label('picture1', 'Image:') !!}</strong>
            {!! Form::file('picture1', ['class' => 'form-control', 'onchange' => 'loadFile(event)']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <!-- Page Field -->
            {!! Form::label('page', 'Page:') !!}
            <select name="page" class="custom-select input-style placeholder-style" id="page" onchange="showdiv(this.value)">
                <option value="">Choisir une Page</option>
                <option value="projectes">Page Réalisation</option>
                <option value="index">Page Accueil</option>
                <option value="actuality">Page Contact</option>
                <option value="contact">Page Actualité</option>
            </select>
        </div>
    </div>
</div>

@if ($slide ?? '')
    <div class="row">
        <div class="col-md-6 output">
            <div class="form-group">
                <img src="{{ $slide->picture1 }}" style="width: 50%; height: 50%; border: none" />
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class="form-group">
                <img style="width: 50%; height: 50%; border: none" id="output" />
            </div>
        </div>
    </div>
@endif
<div class="row mt-3">
    {{--<div class="col-md-6">
        <div class="form-group">
            <input type="checkbox" name="is_free" value="0">
        </div>
    </div>--}}
</div>

<script>
    var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById('output');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };

    function showPrice(val) {
        if (val == 'NON') {
            document.getElementById("entry_free").style.display = 'block';
        } else {
            document.getElementById("entry_free").style.display = 'none';
        }
    }
</script>
@if(isset($slide))
    @if($slide->page == "index")
        <div class="row" id="title_subtitle">
    @else
        <div class="row" id="title" style="display:none;">
    @endif
@else
    <div class="row" id="title" style="display:none;">
@endif
    <div class="row">
        <div class="col-md-6">
            <!-- Titre Field -->
            <div class="form-group">
                {!! Form::label('title', 'Titre:') !!}
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <!-- Picture1 Field -->
        <div class="col-md-6">
            <div class="form-group">
                <strong>{!! Form::label('picture2', 'Image 2(Facultative):') !!}</strong>
                {!! Form::file('picture2', ['class' => 'form-control', 'onchange' => 'loadFile(event)']) !!}
            </div>
        </div>
    </div>

    <!-- Picture1 Field -->
    <div class="col-md-6">
        <div class="form-group">
            <strong>{!! Form::label('picture3', 'Image 3(Facultative):') !!}</strong>
            {!! Form::file('picture3', ['class' => 'form-control', 'onchange' => 'loadFile(event)']) !!}
        </div>
    </div>

</div>


@if(isset($slide))
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <img src="{{ $slide->picture1 }}" style="width: 50%; height: 50%; border: none"/>
            </div>
        </div>
    </div>
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('slides.index') }}" class="btn btn-secondary">Annulé</a>
</div>

<script>
    function showdiv(val){
        if(val == 'index'){
            document.getElementById("title").style.display= 'block';
        }else{
            document.getElementById("title").style.display= 'none';
        }
    }
</script>
