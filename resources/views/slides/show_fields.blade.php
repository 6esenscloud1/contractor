<div class="row mt-3">
    <div class="col-md-6">
        <!-- subtitle At Field -->
        <div class="form-group">
            {!! Form::label('page', 'Page:') !!}
            <p>{{ $slide->page }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('picture1', 'Image:') !!}
            <p>{!! $slide->picture1 !!}</p>
        </div>
    </div>
</div>
@if($slide->page == 'index')
    <div class="row mt-3">
        <div class="col-md-6">
            <!-- subtitle At Field -->
            <div class="form-group">
                {!! Form::label('title', 'Titre:') !!}
                <p>{{ $slide->title }}</p>
            </div>
        </div>
        <div class="col-md-6">
            <!-- subtitle At Field -->
            <div class="form-group">
                {!! Form::label('picture2', 'Image 2:') !!}
                <p>{{ $slide->picture2 }}</p>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-6">
            <!-- subtitle At Field -->
            <div class="form-group">
                {!! Form::label('picture3', 'Image 3:') !!}
                <p>{{ $slide->picture3 }}</p>
            </div>
        </div>
    </div>
@endif

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Date de création:') !!}
            <p>{{ $slide->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Updated At Field -->
        <div class="form-group">
            {!! Form::label('updated_at', 'Date de modification:') !!}
            <p>{{ $slide->updated_at }}</p>
        </div>
    </div>
</div>





