<div class="row mt-3">
    <div class="col-md-4">
        <!-- Name At Field -->
        <div class="form-group">
            {!! Form::label('name', 'Nom & Prénom:') !!}
            <p>{{ $contactRequest->name }}</p>
        </div>
    </div>
    <div class="col-md-4">
       <!-- Email At Field -->
       <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            <p>{{ $contactRequest->email }}</p>
        </div>
    </div>
    <div class="col-md-4">
        <!-- Email At Field -->
        <div class="form-group">
             {!! Form::label('subject', 'Objet:') !!}
             <p>{{ $contactRequest->subject }}</p>
         </div>
     </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('message', 'Message:') !!}
            <p>{!! $contactRequest->message !!}</p>
        </div>
    </div>
</div>


<div class="row mt-3">
    <div class="col-md-6">
        <!-- Phone At Field -->
       <div class="form-group">
            {!! Form::label('phone', 'Téléphone:') !!}
            <p>{{ $contactRequest->phone }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Country_city At Field -->
       <div class="form-group">
            {!! Form::label('country_city', 'Pays/Ville:') !!}
            <p>{{ $contactRequest->country_city }}</p>
        </div>
    </div>
</div>


<div class="row mt-3">
    <div class="col-md-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Date de Création:') !!}
            <p>{{ $contactRequest->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Updated At Field -->
        <div class="form-group">
            {!! Form::label('updated_at', 'Date de Modification:') !!}
            <p>{{ $contactRequest->updated_at }}</p>
        </div>
    </div>
</div>

