@extends('Admin_BackOffice.Admin_layouts.master')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">contact_mail</i>
                            </div>
                            <p class="card-category"> Nombre de Message Reçu :</p>
                            <h3 class="card-title">{{ $countContactRequest }}
                                {{-- <small>GB</small> --}}
                            </h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">visibility</i>
                                <a href="{{ route('contactRequests.index') }}">Voir plus...</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">announcement</i>
                            </div>
                            <p class="card-category"> Nombres d'Actualités :</p>
                            <h3 class="card-title">{{ $countActuality }}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">visibility</i>
                                <a href="{{ route('actualities.index') }}">Voir plus...</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-danger card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">account_tree</i>
                            </div>
                            <p class="card-category"> Nombre de Projets Réalisés :</p>
                            <h3 class="card-title">{{ $countProject }}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">visibility</i>
                                <a href="{{ route('projects.index') }}">Voir plus...</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-chart">
                        <div class="card-header card-header-success">
                            <div class="ct-chart" id="dailySalesChart"></div>
                        </div>
                        <div class="card-body">
                            <h4 class="card-title">Statistique de visites</h4>
                            <p class="card-category">
                                <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">access_time</i> updated 4 minutes ago
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Messages</h4>
                            <p class="card-category"> Tous les messages des clients</p>
                            {{-- <div class="col text-right">
                                <a class="pull-right btn btn-sm btn-light" href="{{ route('orders.create') }}">Ajouter une
                                    commande</a>
                            </div> --}}
                        </div>
                        <div class="card-body">
                            @include('contact_requests.table')
                        </div>
                        <div class="mt-3 d-flex justify-content-center">
                            {{-- {{ $contactRequests->links() }} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
