<div class="row mt-3">
    <div class="col-md-6">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('name', 'Nom:') !!}
            <p>{{ $role->name }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Slug Field -->
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            <p>{{ $role->slug }}</p>
        </div>
    </div>
</div>


<div class="row mt-3">
    <div class="col-md-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Date de création:') !!}
            <p>{{ $role->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Updated At Field -->
        <div class="form-group">
            {!! Form::label('updated_at', 'Date de modification:') !!}
            <p>{{ $role->updated_at }}</p>
        </div>
    </div>
</div>
