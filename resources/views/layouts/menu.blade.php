<li class="nav-item {{ Request::is('trades*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('trades.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Trades</span>
    </a>
</li>
<li class="nav-item {{ Request::is('actualities*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('actualities.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Actualities</span>
    </a>
</li>
<li class="nav-item {{ Request::is('projects*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('projects.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Projects</span>
    </a>
</li>
<li class="nav-item {{ Request::is('contactRequests*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('contactRequests.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Contact Requests</span>
    </a>
</li>
<li class="nav-item {{ Request::is('teams*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('teams.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Teams</span>
    </a>
</li>
<li class="nav-item {{ Request::is('slides*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('slides.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Slides</span>
    </a>
</li>
<li class="nav-item {{ Request::is('roles*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('roles.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Roles</span>
    </a>
</li>
<li class="nav-item {{ Request::is('blocFixes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('blocFixes.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Bloc Fixes</span>
    </a>
</li>
<li class="nav-item {{ Request::is('categoryBlocs*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('categoryBlocs.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Category Blocs</span>
    </a>
</li>
<li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('users.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Users</span>
    </a>
</li>
<li class="nav-item {{ Request::is('settings*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('settings.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Settings</span>
    </a>
</li>
