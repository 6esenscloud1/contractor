<div class="table-responsive-sm">
    <table class="table table-striped" id="settings-table">
        <thead>
            <tr>
                <th>Logo</th>
                <th>Email</th>
                <th>Téléphone</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($settings as $setting)
            <tr>
                <td><img src="{{ asset('/storage/images/' .$setting->logo) }}" alt="{{ $setting->tagline }}" width="90"></td>
                <td>{{ $setting->phone }}</td>
                <td>{{ $setting->email }}</td>
                <td>
                    {!! Form::open(['route' => ['settings.destroy', $setting->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('settings.show', [$setting->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('settings.edit', [$setting->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
