<div class="row mt-3">
    <div class="col-md-6">
        <!-- Email Field -->
        <div class="form-group">
            {!! Form::label('email', 'Email de l&rsquo;entreprise:') !!}
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <!-- Logo Field -->
        <div class="form-group">
            <strong>{!! Form::label('logo', 'Logo de l&rsquo;entreprise:') !!}</strong>
            {!! Form::file('logo', ['class' => 'form-control', 'onchange' => 'loadFile(event)']) !!}
        </div>
    </div>
</div>
@if ($setting ?? '')
    <div class="row">
        <div class="col-md-6 output">
            <div class="form-group">
                <img src="{{ asset('/storage/images/' . $setting->logo) }}" alt="" width="250" height="250">
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class="form-group">
                <img style="width: 50%; height: 50%; border: none" id="output" />
            </div>
        </div>
    </div>
@endif
<div class="row mt-3">
    {{--<div class="col-md-6">
        <div class="form-group">
            <input type="checkbox" name="is_free" value="0">
        </div>
    </div>--}}
</div>

<script>
    var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById('output');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };

    function showPrice(val) {
        if (val == 'NON') {
            document.getElementById("entry_free").style.display = 'block';
        } else {
            document.getElementById("entry_free").style.display = 'none';
        }
    }

</script>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Phone Field -->
        <div class="form-group">
            {!! Form::label('phone', 'Contact de l&rsquo;entreprise:') !!}
            {!! Form::text('phone', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <!-- Geographical_location Field -->
        <div class="form-group">
            {!! Form::label('geographical_location', 'Emplacement géographique:') !!}
            {!! Form::text('geographical_location', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Email Field -->
        <div class="form-group">
            {!! Form::label('tagline', 'Slogan de l&rsquo;entreprise(facultatif):') !!}
            {!! Form::text('tagline', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <!-- Email Field -->
        <div class="form-group">
            {!! Form::label('link_facebook', 'Lien Facebook de l&rsquo;entreprise:') !!}
            {!! Form::text('link_facebook', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Email Field -->
        <div class="form-group">
            {!! Form::label('link_twitter', 'Lien Twitter de l&rsquo;entreprisee:') !!}
            {!! Form::text('link_twitter', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <!-- Email Field -->
        <div class="form-group">
            {!! Form::label('link_linkedin', 'Lien LinkedIn de l&rsquo;entreprise:') !!}
            {!! Form::text('link_linkedin', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('settings.index') }}" class="btn btn-secondary">Annulé</a>
</div>
