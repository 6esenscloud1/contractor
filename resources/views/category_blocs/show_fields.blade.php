<div class="row mt-3">
    <div class="col-md-6">
        <!-- Name At Field -->
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            <p>{{ $categoryBloc->title }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Name At Field -->
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            <p>{{ $categoryBloc->slug }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('message', 'Description:') !!}
            <p>{!! $categoryBloc->description !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Date de création:') !!}
            <p>{{ $categoryBloc->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Updated At Field -->
        <div class="form-group">
            {!! Form::label('updated_at', 'Date de Modification:') !!}
            <p>{{ $categoryBloc->updated_at }}</p>
        </div>
    </div>
</div>




