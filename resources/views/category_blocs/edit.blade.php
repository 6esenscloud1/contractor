{{-- @extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('categoryBlocs.index') !!}">Category Bloc</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Category Bloc</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($categoryBloc, ['route' => ['categoryBlocs.update', $categoryBloc->id], 'method' => 'patch']) !!}

                              @include('category_blocs.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection --}}


@extends('Admin_BackOffice.Admin_layouts.master')

@section('title')
    Nos Catégories | {{ config('app.name') }}
@endsection

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Modifier une catégorie</h4>
                            <p class="card-category">Information sur une catégorie</p>
                        </div>
                        <div class="card-body">
                            {!! Form::model($categoryBloc, ['route' => ['categoryBlocs.update', $categoryBloc->id], 'method' => 'patch', 'files' => true]) !!}

                              @include('category_blocs.fields')

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
