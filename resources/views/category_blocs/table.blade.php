<div class="table-responsive-sm">
    <table class="table table-striped" id="categoryBlocs-table">
        <thead>
            <tr>
                <th>Titre</th>
                <th>Slug</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($categoryBlocs as $categoryBloc)
            <tr>
                <td>{{ $categoryBloc->title }}</td>
                <td>{{ $categoryBloc->slug }}</td>
                <td>
                    {!! Form::open(['route' => ['categoryBlocs.destroy', $categoryBloc->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('categoryBlocs.show', [$categoryBloc->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('categoryBlocs.edit', [$categoryBloc->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
