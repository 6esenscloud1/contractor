<footer class="footer">
    <div class="container-fluid">
        <nav class="float-left">
            <ul>
                <li>
                    <a href="{{url('/')}}">
                        Contractor CI
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright float-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())

            </script>, made with <i class="material-icons">favorite</i> by
            <a href="https://www.agence6sens.fr/" target="_blank">Agence 6ème Sens</a> for a better web.
        </div>
    </div>
</footer>
