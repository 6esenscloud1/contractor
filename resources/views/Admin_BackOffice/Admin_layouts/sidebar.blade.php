<div class="sidebar" data-color="purple" data-background-color="black" data-image="{{ asset('admin/assets/img/sidebar-1.jpg') }}">
    <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
    <div class="logo"><a href="{{url('/')}}" class="simple-text logo-normal">
            <img src="{{ asset('assets/images/logo.png') }}" alt="" width="69%" height="69%">
        </a></div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item {{ Request::is('home*') ? 'active' : '' }} ">
                <a class="nav-link" href="{{ route('home') }}">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item {{ Request::is('trades*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('trades.index') }}">
                    <i class="material-icons">work</i>
                    <p>Nos métiers</p>
                </a>
            </li>
            <li class="nav-item {{ Request::is('actualities*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('actualities.index') }}">
                    <i class="material-icons">announcement</i>
                    <p>Nos actualités</p>
                </a>
            </li>
            <li class="nav-item {{ Request::is('slides*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('slides.index') }}">
                    <i class="material-icons">slideshow</i>
                    <p>Gestion du slide</p>
                </a>
            </li>
            <li class="nav-item {{ Request::is('projects*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('projects.index') }}">
                    <i class="material-icons">account_tree</i>
                    <p>Nos réalisations</p>
                </a>
            </li>
            <li class="nav-item {{ Request::is('teams*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('teams.index') }}">
                    <i class="material-icons">people_alt</i>
                    <p>La Teams</p>
                </a>
            </li>
             <li class="nav-item {{ Request::is('contactRequests*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('contactRequests.index') }}">
                    <i class="material-icons">contact_mail</i>
                    <p>Formulaire de Contact</p>
                </a>
            </li>
            <li class="nav-item  {{ Request::is('categoryBlocs*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('categoryBlocs.index') }}">
                    <i class="material-icons">category</i>
                    <p>Categorie RSE</p>
                </a>
            </li>
             <li class="nav-item  {{ Request::is('blocFixes*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('blocFixes.index') }}">
                    <i class="material-icons">notifications</i>
                    <p>Le RSE</p>
                </a>
            </li>
            <li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('users.index') }}">
                    <i class="material-icons">person</i>
                    <p>Gestion des Utilisateurs</p>
                </a>
            </li>
            <li class="nav-item {{ Request::is('roles*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('roles.index') }}">
                    <i class="material-icons">functions</i>
                    <p>Rôle des Utilisateurs</p>
                </a>
            </li>
            {{-- <li class="nav-item {{ Request::is('settings*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('settings.index') }}">
                    <i class="material-icons">language</i>
                    <p>Setting</p>
                </a>
            </li> --}}
        </ul>
    </div>
</div>
