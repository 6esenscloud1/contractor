{{-- @extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Bloc Fixes</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             BlocFixes
                             <a class="pull-right" href="{{ route('blocFixes.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                         </div>
                         <div class="card-body">
                             @include('bloc_fixes.table')
                              <div class="pull-right mr-3">

                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection --}}

@extends('Admin_BackOffice.Admin_layouts.master')

@section('title')
    Les blocs RSE | {{ config('app.name') }}
@endsection

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="clearfix"></div>

            @include('flash::message')

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title mt-0"> Les Blocs RSE</h4>
                            <p class="card-category"> Tous les Blocs RSE</p>
                            <div class="col text-right">
                                <a class="pull-right btn btn-sm btn-light" href="{{ route('blocFixes.create') }}">Ajouter un
                                bloc</a>
                            </div>
                        </div>
                        <div class="card-body">
                            @include('bloc_fixes.table')
                        </div>
                        <div class="mt-3 d-flex justify-content-center">
                        {{--  {{ $actualities->links() }}  --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

