{{-- @extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('blocFixes.index') }}">Bloc Fix</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('blocFixes.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('bloc_fixes.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection --}}


@extends('Admin_BackOffice.Admin_layouts.master')

@section('title')
   Bloc Fix | {{ config('app.name') }}
@endsection


@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Détails du Blo</h4>
                            <p class="card-category">Information sur le Bloc</p>
                        </div>
                        <div class="card-body">
                            {{-- <div class="row"> --}}
                                @include('bloc_fixes.show_fields')
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <a href="{{ route('blocFixes.edit', [$blocFix->id]) }}" class="btn btn-primary pull-right">Modifier</a>
                                    </div>
                                    <div class="form-group col-sm-2">
                                        <a href="{{ route('blocFixes.index') }}" class="btn btn-primary pull-right">Retour</a>
                                    </div>
                                </div>
                            {{-- </div> --}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
