<div class="row mt-3">
    <div class="col-md-6">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
       <!-- Category_id Field -->
       <div class="form-group">
        {!! Form::label('category_id', 'Les Catégories :') !!}
        <select name="category_id" class="custom-select input-style placeholder-style" id="category_id">
            <option value="">Choisir une Catégorie</option>
            @foreach($categoryBlocs as $categoryBloc)
                <option value="{{$categoryBloc->id}}">{{$categoryBloc->title}}</option>
            @endforeach
        </select>
    </div>
    </div>
</div>


<div class="row mt-3">
    <!-- description Field -->
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'name' => 'description']) !!}
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('blocFixes.index') }}" class="btn btn-secondary">Annulé</a>
</div>
