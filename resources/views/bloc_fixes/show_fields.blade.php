<div class="row mt-3">
    <div class="col-md-6">
        <!-- Name At Field -->
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            <p>{{ $blocFix->title }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Name At Field -->
        <div class="form-group">
            {!! Form::label('category_id', 'Catégorie de Bloc:') !!}
            <p>{{ $blocFix->getCategorieBlocAttribute()->title ?? '---' }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('message', 'Description:') !!}
            <p>{!! $blocFix->description !!}</p>
        </div>
    </div>
</div>


<div class="row mt-3">
    <div class="col-md-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Date de Création:') !!}
            <p>{{ $blocFix->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Updated At Field -->
        <div class="form-group">
            {!! Form::label('updated_at', 'Date de Modification:') !!}
            <p>{{ $blocFix->updated_at }}</p>
        </div>
    </div>
</div>




