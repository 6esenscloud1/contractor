<div class="row mt-3">
    <div class="col-md-6">
        <!-- title At Field -->
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            <p>{{ $project->title }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- title At Field -->
        <div class="form-group">
            {!! Form::label('slug', 'Titre:') !!}
            <p>{{ $project->slug }}</p>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-6">
        <!-- title At Field -->
        <div class="form-group">
            {!! Form::label('project_owner', 'Maître d&rsquo;Ouvrage:') !!}
            <p>{{ $project->project_owner ?? '---' }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- title At Field -->
        <div class="form-group">
            {!! Form::label('project_manager', 'Maître d&rsquo;Oeuvre:') !!}
            <p>{{ $project->project_manager ?? '---' }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            <p>{!! $project->description !!}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <!-- Image At Field -->
        <div class="form-group">
            {!! Form::label('image', 'Image de garde:') !!}
            <p><img src="{{ asset('/storage/images/' . $project->image) }}" alt="" width="150" height="150"></p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- start_of_work At Field -->
        <div class="form-group">
            {!! Form::label('start_of_work', 'Date de début:') !!}
            <p>{{ $project->start_of_work }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- end_of_work At Field -->
        <div class="form-group">
            {!! Form::label('end_of_work', 'Date de fin:') !!}
            <p>{{ $project->end_of_work }}</p>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <!-- Image At Field -->
        <div class="form-group">
            {!! Form::label('medias', 'Autres images:') !!}
            @if ($project->file == null)
                @php
                $files = explode(',',trim(stripslashes($project->medias),"[]"));
                @endphp
                @foreach ($files as $item)
                    @php
                    $item = trim($item, '"');
                    $url = "/storage/$item";
                    $arr = explode('/', $url);
                    $last = end($arr);
                    $download_link = "/download/$last";
                    @endphp
                    <a href="{{ url($download_link) }}">
                        <img src="{{ asset($url) }}" width="25%"/>
                    </a>
                @endforeach
            @else
                Aucune image de disponible
            @endif
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Date de Création:') !!}
            <p>{{ $project->created_at }}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Updated At Field -->
        <div class="form-group">
            {!! Form::label('updated_at', 'Date de modification:') !!}
            <p>{{ $project->updated_at }}</p>
        </div>
    </div>
</div>

