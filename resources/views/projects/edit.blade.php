{{-- @extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('projects.index') !!}">Project</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Project</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($project, ['route' => ['projects.update', $project->id], 'method' => 'patch']) !!}

                              @include('projects.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection --}}


@extends('Admin_BackOffice.Admin_layouts.master')

@section('title')
    Nos réalisations | {{ config('app.name') }}
@endsection

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Modifier une réalisation</h4>
                            <p class="card-category">Information sur la réalisation</p>
                        </div>
                        <div class="card-body">
                            {!! Form::model($project, ['route' => ['projects.update', $project->id], 'method' => 'patch', 'files' => true]) !!}

                              @include('projects.fields')

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
