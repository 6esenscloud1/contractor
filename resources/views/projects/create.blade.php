{{-- @extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('projects.index') !!}">Project</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Project</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'projects.store']) !!}

                                   @include('projects.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection --}}


@extends('Admin_BackOffice.Admin_layouts.master')

@section('title')
    Nos Projets | {{ config('app.name') }}
@endsection


@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Créer un projet</h4>
                            <p class="card-category">Information sur les projets</p>
                        </div>
                        <div class="card-body">

                            {!! Form::open(['route' => 'projects.store', 'files' => true]) !!}

                            @include('projects.fields')

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
