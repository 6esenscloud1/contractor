<div class="row mt-3">
    <div class="col-md-6">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('title', 'Titre:') !!}
            {!! Form::text('title', null, ['class' => 'form-control', 'onkeyup' => 'stringToSlug(this.value)']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control', 'readonly']) !!}
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-6">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('project_owner', 'Maître d&rsquo;Ouvrage:') !!}
            {!! Form::text('project_owner', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('project_manager', 'Maître d&rsquo;Oeuvre:') !!}
            {!! Form::text('project_manager', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- description Field -->
<div class="row mt-3">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'name' => 'description']) !!}
        </div>
    </div>
</div>


<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            <strong>{!! Form::label('image', 'Image de Présentation:') !!}</strong>
            {!! Form::file('image', ['class' => 'form-control', 'onchange' => 'loadFile(event)']) !!}
        </div>
    </div>
</div>
@if ($project ?? '')
    <div class="row">
        <div class="col-md-6 output">
            <div class="form-group">
                <img src="{{ asset('/storage/images/' . $project->image) }}" alt="{{ $project->title }}" width="250" height="250" />
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class="form-group">
                <img style="width: 50%; height: 50%; border: none" id="output" />
            </div>
        </div>
    </div>
@endif
<div class="row mt-3">
    {{--<div class="col-md-6">
        <div class="form-group">
            <input type="checkbox" name="is_free" value="0">
        </div>
    </div>--}}
</div>

<script>
    var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById('output');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };

    function showPrice(val) {
        if (val == 'NON') {
            document.getElementById("entry_free").style.display = 'block';
        } else {
            document.getElementById("entry_free").style.display = 'none';
        }
    }

</script>


<div class="row mt-3">
    <!-- start_of_work Field -->
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('start_of_work', 'Date de debut:') !!}
            @if(isset($project))
                {!! Form::date('start_of_work', \Carbon\Carbon::parse($project->start_of_work)->format('Y-m-d') ?? null, ['class' => 'form-control', 'id' => 'start_of_work','value' => 0]) !!}
            @else
                {!! Form::date('start_of_work', null, ['class' => 'form-control', 'id' => 'start_of_work','value' => 0]) !!}
            @endif
        </div>
    </div>
    <!-- image Field -->
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('end_of_work', 'Date de fin:') !!}
            @if(isset($project))
                {!! Form::date('end_of_work', \Carbon\Carbon::parse($project->end_of_work)->format('Y-m-d') ?? null, ['class' => 'form-control', 'id' => 'end_of_work','value' => 0]) !!}
            @else
                {!! Form::date('end_of_work', null, ['class' => 'form-control', 'id' => 'end_of_work','value' => 0]) !!}
            @endif
        </div>
    </div>
</div>


<div class="row mt-3">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('medias', 'Autres Images:') !!}
            {!! Form::file('medias[]', ['class' => 'form-control', 'onchange' => 'loadFile(this)', 'multiple'=>true,'accept'=>'image/*']) !!}
        </div>
    </div>
</div>
@if ($project ?? '')
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <img style="width: 50%; height: 50%; border: none" />
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-md-6 gallery">
            <div class="form-group">
                <img style="width: 50; height: 50; border: none" id="gallery" />
            </div>
        </div>
    </div>
@endif
<div class="row mt-3">
    {{--<div class="col-md-6">
        <div class="form-group">
            <input type="checkbox" name="is_free" value="0">
        </div>
    </div>--}}
</div>


<script>
    var loadFile = function(input){
        if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo('.gallery');
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }
    // var loadFile = function(event) {
    //     var reader = new FileReader();
    //     reader.onload = function() {
    //         var output = document.getElementById('output');
    //         output.src = reader.result;
    //     };
    //     reader.readAsDataURL(event.target.files[0]);
    // };
</script>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistré', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('projects.index') }}" class="btn btn-secondary">Annulé</a>
</div>

<script>
    function stringToSlug(str) {

        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        // return str;
        document.getElementById("slug").value = str;
    }

    function showPercentage(val){
        if(val == 1){
            document.getElementById("div_percentage").style.display= 'block';
        }else{
            document.getElementById("div_percentage").style.display= 'none';
        }
    }

    function showSold(val){
        if(val == 1){
            document.getElementById("div_sold").style.display= 'block';
        }else{
            document.getElementById("div_sold").style.display= 'none';
        }
    }
</script>
