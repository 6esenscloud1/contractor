<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CategoryBloc;

class CategoryBlocApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_category_bloc()
    {
        $categoryBloc = CategoryBloc::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/category_blocs', $categoryBloc
        );

        $this->assertApiResponse($categoryBloc);
    }

    /**
     * @test
     */
    public function test_read_category_bloc()
    {
        $categoryBloc = CategoryBloc::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/category_blocs/'.$categoryBloc->id
        );

        $this->assertApiResponse($categoryBloc->toArray());
    }

    /**
     * @test
     */
    public function test_update_category_bloc()
    {
        $categoryBloc = CategoryBloc::factory()->create();
        $editedCategoryBloc = CategoryBloc::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/category_blocs/'.$categoryBloc->id,
            $editedCategoryBloc
        );

        $this->assertApiResponse($editedCategoryBloc);
    }

    /**
     * @test
     */
    public function test_delete_category_bloc()
    {
        $categoryBloc = CategoryBloc::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/category_blocs/'.$categoryBloc->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/category_blocs/'.$categoryBloc->id
        );

        $this->response->assertStatus(404);
    }
}
