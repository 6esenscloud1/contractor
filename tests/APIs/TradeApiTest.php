<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Trade;

class TradeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_trade()
    {
        $trade = Trade::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/trades', $trade
        );

        $this->assertApiResponse($trade);
    }

    /**
     * @test
     */
    public function test_read_trade()
    {
        $trade = Trade::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/trades/'.$trade->id
        );

        $this->assertApiResponse($trade->toArray());
    }

    /**
     * @test
     */
    public function test_update_trade()
    {
        $trade = Trade::factory()->create();
        $editedTrade = Trade::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/trades/'.$trade->id,
            $editedTrade
        );

        $this->assertApiResponse($editedTrade);
    }

    /**
     * @test
     */
    public function test_delete_trade()
    {
        $trade = Trade::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/trades/'.$trade->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/trades/'.$trade->id
        );

        $this->response->assertStatus(404);
    }
}
