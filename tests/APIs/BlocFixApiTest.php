<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\BlocFix;

class BlocFixApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_bloc_fix()
    {
        $blocFix = BlocFix::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/bloc_fixes', $blocFix
        );

        $this->assertApiResponse($blocFix);
    }

    /**
     * @test
     */
    public function test_read_bloc_fix()
    {
        $blocFix = BlocFix::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/bloc_fixes/'.$blocFix->id
        );

        $this->assertApiResponse($blocFix->toArray());
    }

    /**
     * @test
     */
    public function test_update_bloc_fix()
    {
        $blocFix = BlocFix::factory()->create();
        $editedBlocFix = BlocFix::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/bloc_fixes/'.$blocFix->id,
            $editedBlocFix
        );

        $this->assertApiResponse($editedBlocFix);
    }

    /**
     * @test
     */
    public function test_delete_bloc_fix()
    {
        $blocFix = BlocFix::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/bloc_fixes/'.$blocFix->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/bloc_fixes/'.$blocFix->id
        );

        $this->response->assertStatus(404);
    }
}
