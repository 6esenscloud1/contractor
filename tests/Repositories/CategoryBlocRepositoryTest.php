<?php namespace Tests\Repositories;

use App\Models\CategoryBloc;
use App\Repositories\CategoryBlocRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CategoryBlocRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CategoryBlocRepository
     */
    protected $categoryBlocRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->categoryBlocRepo = \App::make(CategoryBlocRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_category_bloc()
    {
        $categoryBloc = CategoryBloc::factory()->make()->toArray();

        $createdCategoryBloc = $this->categoryBlocRepo->create($categoryBloc);

        $createdCategoryBloc = $createdCategoryBloc->toArray();
        $this->assertArrayHasKey('id', $createdCategoryBloc);
        $this->assertNotNull($createdCategoryBloc['id'], 'Created CategoryBloc must have id specified');
        $this->assertNotNull(CategoryBloc::find($createdCategoryBloc['id']), 'CategoryBloc with given id must be in DB');
        $this->assertModelData($categoryBloc, $createdCategoryBloc);
    }

    /**
     * @test read
     */
    public function test_read_category_bloc()
    {
        $categoryBloc = CategoryBloc::factory()->create();

        $dbCategoryBloc = $this->categoryBlocRepo->find($categoryBloc->id);

        $dbCategoryBloc = $dbCategoryBloc->toArray();
        $this->assertModelData($categoryBloc->toArray(), $dbCategoryBloc);
    }

    /**
     * @test update
     */
    public function test_update_category_bloc()
    {
        $categoryBloc = CategoryBloc::factory()->create();
        $fakeCategoryBloc = CategoryBloc::factory()->make()->toArray();

        $updatedCategoryBloc = $this->categoryBlocRepo->update($fakeCategoryBloc, $categoryBloc->id);

        $this->assertModelData($fakeCategoryBloc, $updatedCategoryBloc->toArray());
        $dbCategoryBloc = $this->categoryBlocRepo->find($categoryBloc->id);
        $this->assertModelData($fakeCategoryBloc, $dbCategoryBloc->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_category_bloc()
    {
        $categoryBloc = CategoryBloc::factory()->create();

        $resp = $this->categoryBlocRepo->delete($categoryBloc->id);

        $this->assertTrue($resp);
        $this->assertNull(CategoryBloc::find($categoryBloc->id), 'CategoryBloc should not exist in DB');
    }
}
