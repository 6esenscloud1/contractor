<?php namespace Tests\Repositories;

use App\Models\BlocFix;
use App\Repositories\BlocFixRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class BlocFixRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var BlocFixRepository
     */
    protected $blocFixRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->blocFixRepo = \App::make(BlocFixRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_bloc_fix()
    {
        $blocFix = BlocFix::factory()->make()->toArray();

        $createdBlocFix = $this->blocFixRepo->create($blocFix);

        $createdBlocFix = $createdBlocFix->toArray();
        $this->assertArrayHasKey('id', $createdBlocFix);
        $this->assertNotNull($createdBlocFix['id'], 'Created BlocFix must have id specified');
        $this->assertNotNull(BlocFix::find($createdBlocFix['id']), 'BlocFix with given id must be in DB');
        $this->assertModelData($blocFix, $createdBlocFix);
    }

    /**
     * @test read
     */
    public function test_read_bloc_fix()
    {
        $blocFix = BlocFix::factory()->create();

        $dbBlocFix = $this->blocFixRepo->find($blocFix->id);

        $dbBlocFix = $dbBlocFix->toArray();
        $this->assertModelData($blocFix->toArray(), $dbBlocFix);
    }

    /**
     * @test update
     */
    public function test_update_bloc_fix()
    {
        $blocFix = BlocFix::factory()->create();
        $fakeBlocFix = BlocFix::factory()->make()->toArray();

        $updatedBlocFix = $this->blocFixRepo->update($fakeBlocFix, $blocFix->id);

        $this->assertModelData($fakeBlocFix, $updatedBlocFix->toArray());
        $dbBlocFix = $this->blocFixRepo->find($blocFix->id);
        $this->assertModelData($fakeBlocFix, $dbBlocFix->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_bloc_fix()
    {
        $blocFix = BlocFix::factory()->create();

        $resp = $this->blocFixRepo->delete($blocFix->id);

        $this->assertTrue($resp);
        $this->assertNull(BlocFix::find($blocFix->id), 'BlocFix should not exist in DB');
    }
}
