<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Artisan::call('storage:link');


Route::get('/', [App\Http\Controllers\CustomerFrontOfficeController::class, 'indexFront']);

Route::get('/about', [App\Http\Controllers\CustomerFrontOfficeController::class, "about"])->name('about');

Route::get('/contact', [App\Http\Controllers\CustomerFrontOfficeController::class, "contacts"])->name('contact');

Route::get('/team', [App\Http\Controllers\CustomerFrontOfficeController::class, "teams"])->name('team');

Route::get('/actuality', [App\Http\Controllers\CustomerFrontOfficeController::class, "indexActualities"])->name('actuality');

Route::get('/show_actuality/{slug}', [App\Http\Controllers\CustomerFrontOfficeController::class, "showActualities"]);

Route::get('/projectes', [App\Http\Controllers\CustomerFrontOfficeController::class, "indexProjects"])->name('projectes');

Route::get('/show_project/{slug}', [App\Http\Controllers\CustomerFrontOfficeController::class, "showProjects"])->name('showproject');

Route::post('ckeditor/upload', [App\Http\Controllers\CKEditorController::class, "upload"])->name('upload');

Auth::routes();

Route::post('/logout', [App\Http\Controllers\Auth\LoginController::class, "logout"])->name('logout');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('trades', App\Http\Controllers\TradeController::class);

Route::resource('actualities', App\Http\Controllers\ActualityController::class);

Route::resource('projects', App\Http\Controllers\ProjectController::class);

Route::resource('contactRequests', App\Http\Controllers\ContactRequestController::class);

Route::post('/contacter', [App\Http\Controllers\ContactRequestController::class, "addContactRequest"])->name('contacter');

Route::resource('teams', App\Http\Controllers\TeamController::class);

Route::resource('slides', App\Http\Controllers\SlideController::class);

Route::resource('roles', App\Http\Controllers\RoleController::class);

Route::resource('blocFixes', App\Http\Controllers\BlocFixController::class);

Route::resource('categoryBlocs', App\Http\Controllers\CategoryBlocController::class);

Route::resource('users', App\Http\Controllers\UserController::class);

Route::resource('settings', App\Http\Controllers\SettingController::class);
