<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('trades', App\Http\Controllers\API\TradeAPIController::class);

Route::resource('actualities', App\Http\Controllers\API\ActualityAPIController::class);

Route::resource('projects', App\Http\Controllers\API\ProjectAPIController::class);

Route::resource('contact_requests', App\Http\Controllers\API\ContactRequestAPIController::class);

Route::resource('teams', App\Http\Controllers\API\TeamAPIController::class);

Route::resource('slides', App\Http\Controllers\API\SlideAPIController::class);

Route::resource('roles', App\Http\Controllers\API\RoleAPIController::class);

Route::resource('bloc_fixes', App\Http\Controllers\API\BlocFixAPIController::class);

Route::resource('category_blocs', App\Http\Controllers\API\CategoryBlocAPIController::class);

Route::resource('users', App\Http\Controllers\API\UserAPIController::class);

Route::resource('settings', App\Http\Controllers\API\SettingAPIController::class);