<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Repositories\ProjectRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Project;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProjectController extends AppBaseController
{
    /** @var  ProjectRepository */
    private $projectRepository;

    public function __construct(ProjectRepository $projectRepo)
    {
        $this->projectRepository = $projectRepo;
    }

    /**
     * Display a listing of the Project.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $projects = $this->projectRepository->all();
        $projects = Project::orderBy('id', 'desc')->paginate(10);

        return view('projects.index')
            ->with('projects', $projects);
    }

    /**
     * Show the form for creating a new Project.
     *
     * @return Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created Project in storage.
     *
     * @param CreateProjectRequest $request
     *
     * @return Response
     */
    public function store(CreateProjectRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('image'))
        {
            $destination_path = 'public/images';
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('image')->storeAs($destination_path, $image_name);

            $input['image'] = $image_name;
        }


        if ($request->file('medias')) {

            $pictures = [];
            $image = $request->file('medias');
            if ($image) {
                for ($i = 0; $i < sizeof($image); $i++) {
                    ${'image' . ($i)} = $image[$i]->store('storage/images', ['disk' => 'public']);
                    ${'picture' . ($i)} = ${'image' . ($i)} ?? null;
                    array_push($pictures, ${'picture' . ($i)});
                }
                // dd($pictures);
            }

        } else {
            $pictures = null;
        }

        $projectImageLink = json_encode($pictures);

        $input["medias"] = $projectImageLink;

        $project = $this->projectRepository->create($input);

        Flash::success('Project saved successfully.');

        return redirect(route('projects.index'));
    }

    /**
     * Display the specified Project.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $project = $this->projectRepository->find($id);

        if (empty($project)) {
            Flash::error('Project not found');

            return redirect(route('projects.index'));
        }


        return view('projects.show')->with('project', $project);
    }

    /**
     * Show the form for editing the specified Project.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $project = $this->projectRepository->find($id);

        if (empty($project)) {
            Flash::error('Project not found');

            return redirect(route('projects.index'));
        }

        return view('projects.edit')->with('project', $project);
    }

    /**
     * Update the specified Project in storage.
     *
     * @param int $id
     * @param UpdateProjectRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProjectRequest $request)
    {
        $project = $this->projectRepository->find($id);

        if (empty($project)) {
            Flash::error('Project not found');

            return redirect(route('projects.index'));
        }

        $input = $request->all();

        if ($request->hasFile('image'))
        {
            $destination_path = 'public/images';
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('image')->storeAs($destination_path, $image_name);

            $input['image'] = $image_name;
        }


        if ($request->file('medias')) {

            $pictures = [];
            $image = $request->file('medias');
            if ($image) {
                for ($i = 0; $i < sizeof($image); $i++) {
                    ${'image' . ($i)} = $image[$i]->store('public/images', ['disk' => 'public']);
                    ${'picture' . ($i)} = ${'image' . ($i)} ?? null;
                    array_push($pictures, ${'picture' . ($i)});
                }
                // dd($pictures);
            }

        } else {
            $pictures = null;
        }

        $projectImageLink = json_encode($pictures);

        $input["medias"] = $projectImageLink;

        $input['start_of_work'] = $project->start_of_work;
        $input['end_of_work'] = $project->end_of_work;

        $project = $this->projectRepository->update($input, $id);

        Flash::success('Project updated successfully.');

        return redirect(route('projects.index'));
    }

    /**
     * Remove the specified Project from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $project = $this->projectRepository->find($id);

        if (empty($project)) {
            Flash::error('Project not found');

            return redirect(route('projects.index'));
        }

        $this->projectRepository->delete($id);

        Flash::success('Project deleted successfully.');

        return redirect(route('projects.index'));
    }
}
