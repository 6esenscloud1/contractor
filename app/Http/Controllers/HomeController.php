<?php

namespace App\Http\Controllers;

use App\Models\Actuality;
use App\Models\ContactRequest;
use App\Models\Project;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $contactRequests = ContactRequest::all();
        $countContactRequest = count($contactRequests);

        $actualities = Actuality::all();
        $countActuality = count($actualities);

        $projects = Project::all();
        $countProject = count($projects);


        return view('home', compact('countContactRequest', 'countActuality', 'countProject', 'contactRequests'));
    }
}
