<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryBlocRequest;
use App\Http\Requests\UpdateCategoryBlocRequest;
use App\Repositories\CategoryBlocRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\CategoryBloc;
use Illuminate\Http\Request;
use Flash;
use Response;

class CategoryBlocController extends AppBaseController
{
    /** @var  CategoryBlocRepository */
    private $categoryBlocRepository;

    public function __construct(CategoryBlocRepository $categoryBlocRepo)
    {
        $this->categoryBlocRepository = $categoryBlocRepo;
    }

    /**
     * Display a listing of the CategoryBloc.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $categoryBlocs = $this->categoryBlocRepository->all();
        $categoryBlocs = CategoryBloc::orderBy('id', 'desc')->paginate(10);

        return view('category_blocs.index')
            ->with('categoryBlocs', $categoryBlocs);
    }

    /**
     * Show the form for creating a new CategoryBloc.
     *
     * @return Response
     */
    public function create()
    {
        return view('category_blocs.create');
    }

    /**
     * Store a newly created CategoryBloc in storage.
     *
     * @param CreateCategoryBlocRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryBlocRequest $request)
    {
        $input = $request->all();

        $categoryBloc = $this->categoryBlocRepository->create($input);

        Flash::success('Category Bloc saved successfully.');

        return redirect(route('categoryBlocs.index'));
    }

    /**
     * Display the specified CategoryBloc.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categoryBloc = $this->categoryBlocRepository->find($id);

        if (empty($categoryBloc)) {
            Flash::error('Category Bloc not found');

            return redirect(route('categoryBlocs.index'));
        }

        return view('category_blocs.show')->with('categoryBloc', $categoryBloc);
    }

    /**
     * Show the form for editing the specified CategoryBloc.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categoryBloc = $this->categoryBlocRepository->find($id);

        if (empty($categoryBloc)) {
            Flash::error('Category Bloc not found');

            return redirect(route('categoryBlocs.index'));
        }

        return view('category_blocs.edit')->with('categoryBloc', $categoryBloc);
    }

    /**
     * Update the specified CategoryBloc in storage.
     *
     * @param int $id
     * @param UpdateCategoryBlocRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryBlocRequest $request)
    {
        $categoryBloc = $this->categoryBlocRepository->find($id);

        if (empty($categoryBloc)) {
            Flash::error('Category Bloc not found');

            return redirect(route('categoryBlocs.index'));
        }

        $categoryBloc = $this->categoryBlocRepository->update($request->all(), $id);

        Flash::success('Category Bloc updated successfully.');

        return redirect(route('categoryBlocs.index'));
    }

    /**
     * Remove the specified CategoryBloc from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categoryBloc = $this->categoryBlocRepository->find($id);

        if (empty($categoryBloc)) {
            Flash::error('Category Bloc not found');

            return redirect(route('categoryBlocs.index'));
        }

        $this->categoryBlocRepository->delete($id);

        Flash::success('Category Bloc deleted successfully.');

        return redirect(route('categoryBlocs.index'));
    }
}
