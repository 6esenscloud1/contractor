<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCategoryBlocAPIRequest;
use App\Http\Requests\API\UpdateCategoryBlocAPIRequest;
use App\Models\CategoryBloc;
use App\Repositories\CategoryBlocRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CategoryBlocController
 * @package App\Http\Controllers\API
 */

class CategoryBlocAPIController extends AppBaseController
{
    /** @var  CategoryBlocRepository */
    private $categoryBlocRepository;

    public function __construct(CategoryBlocRepository $categoryBlocRepo)
    {
        $this->categoryBlocRepository = $categoryBlocRepo;
    }

    /**
     * Display a listing of the CategoryBloc.
     * GET|HEAD /categoryBlocs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $categoryBlocs = $this->categoryBlocRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($categoryBlocs->toArray(), 'Category Blocs retrieved successfully');
    }

    /**
     * Store a newly created CategoryBloc in storage.
     * POST /categoryBlocs
     *
     * @param CreateCategoryBlocAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryBlocAPIRequest $request)
    {
        $input = $request->all();

        $categoryBloc = $this->categoryBlocRepository->create($input);

        return $this->sendResponse($categoryBloc->toArray(), 'Category Bloc saved successfully');
    }

    /**
     * Display the specified CategoryBloc.
     * GET|HEAD /categoryBlocs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CategoryBloc $categoryBloc */
        $categoryBloc = $this->categoryBlocRepository->find($id);

        if (empty($categoryBloc)) {
            return $this->sendError('Category Bloc not found');
        }

        return $this->sendResponse($categoryBloc->toArray(), 'Category Bloc retrieved successfully');
    }

    /**
     * Update the specified CategoryBloc in storage.
     * PUT/PATCH /categoryBlocs/{id}
     *
     * @param int $id
     * @param UpdateCategoryBlocAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryBlocAPIRequest $request)
    {
        $input = $request->all();

        /** @var CategoryBloc $categoryBloc */
        $categoryBloc = $this->categoryBlocRepository->find($id);

        if (empty($categoryBloc)) {
            return $this->sendError('Category Bloc not found');
        }

        $categoryBloc = $this->categoryBlocRepository->update($input, $id);

        return $this->sendResponse($categoryBloc->toArray(), 'CategoryBloc updated successfully');
    }

    /**
     * Remove the specified CategoryBloc from storage.
     * DELETE /categoryBlocs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CategoryBloc $categoryBloc */
        $categoryBloc = $this->categoryBlocRepository->find($id);

        if (empty($categoryBloc)) {
            return $this->sendError('Category Bloc not found');
        }

        $categoryBloc->delete();

        return $this->sendSuccess('Category Bloc deleted successfully');
    }
}
