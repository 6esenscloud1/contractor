<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBlocFixAPIRequest;
use App\Http\Requests\API\UpdateBlocFixAPIRequest;
use App\Models\BlocFix;
use App\Repositories\BlocFixRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class BlocFixController
 * @package App\Http\Controllers\API
 */

class BlocFixAPIController extends AppBaseController
{
    /** @var  BlocFixRepository */
    private $blocFixRepository;

    public function __construct(BlocFixRepository $blocFixRepo)
    {
        $this->blocFixRepository = $blocFixRepo;
    }

    /**
     * Display a listing of the BlocFix.
     * GET|HEAD /blocFixes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $blocFixes = $this->blocFixRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($blocFixes->toArray(), 'Bloc Fixes retrieved successfully');
    }

    /**
     * Store a newly created BlocFix in storage.
     * POST /blocFixes
     *
     * @param CreateBlocFixAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBlocFixAPIRequest $request)
    {
        $input = $request->all();

        $blocFix = $this->blocFixRepository->create($input);

        return $this->sendResponse($blocFix->toArray(), 'Bloc Fix saved successfully');
    }

    /**
     * Display the specified BlocFix.
     * GET|HEAD /blocFixes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BlocFix $blocFix */
        $blocFix = $this->blocFixRepository->find($id);

        if (empty($blocFix)) {
            return $this->sendError('Bloc Fix not found');
        }

        return $this->sendResponse($blocFix->toArray(), 'Bloc Fix retrieved successfully');
    }

    /**
     * Update the specified BlocFix in storage.
     * PUT/PATCH /blocFixes/{id}
     *
     * @param int $id
     * @param UpdateBlocFixAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBlocFixAPIRequest $request)
    {
        $input = $request->all();

        /** @var BlocFix $blocFix */
        $blocFix = $this->blocFixRepository->find($id);

        if (empty($blocFix)) {
            return $this->sendError('Bloc Fix not found');
        }

        $blocFix = $this->blocFixRepository->update($input, $id);

        return $this->sendResponse($blocFix->toArray(), 'BlocFix updated successfully');
    }

    /**
     * Remove the specified BlocFix from storage.
     * DELETE /blocFixes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BlocFix $blocFix */
        $blocFix = $this->blocFixRepository->find($id);

        if (empty($blocFix)) {
            return $this->sendError('Bloc Fix not found');
        }

        $blocFix->delete();

        return $this->sendSuccess('Bloc Fix deleted successfully');
    }
}
