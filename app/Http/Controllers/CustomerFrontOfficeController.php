<?php

namespace App\Http\Controllers;

use App\Models\Actuality;
use App\Models\BlocFix;
use App\Models\CategoryBloc;
use App\Models\Project;
use App\Models\Slide;
use App\Models\Team;
use App\Models\Trade;
use Illuminate\Http\Request;

class CustomerFrontOfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexFront()
    {
        $trades = Trade::take(3)->get();

        $bloc_fixs = BlocFix::take(3)->get();

        $category_blocs = CategoryBloc::take(1)->get();

        $actualities = Actuality::take(3)->get();

        $slides = Slide::take(3)->get();

        return view('Customer_FrontOffice.index', compact('actualities', 'trades', 'bloc_fixs', 'category_blocs', 'slides'));
    }


    public function about()
    {
        return view('Customer_FrontOffice.about');
    }

    public function teams()
    {
        $teams = Team::take(3)->get();
        return view('Customer_FrontOffice.teams', compact('teams'));
    }

    public function contacts()
    {
        return view('Customer_FrontOffice.contacts');
    }

    public function indexActualities()
    {
        $actualities = Actuality::all();

        return view('Customer_FrontOffice.actuallity.index', compact('actualities'));
    }

    public function showActualities($slug)
    {
        $actualitie = Actuality::where('slug', $slug)->first();

        return view('Customer_FrontOffice.actuallity.show', compact('actualitie'));
    }

    public function indexProjects()
    {
        $projects = Project::all();

        return view('Customer_FrontOffice.projects.index', compact('projects'));
    }

    public function showProjects($slug)
    {
        $project = Project::where('slug', $slug)->first();

        return view('Customer_FrontOffice.projects.show', compact('project'));
    }

}
