<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBlocFixRequest;
use App\Http\Requests\UpdateBlocFixRequest;
use App\Repositories\BlocFixRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\BlocFix;
use App\Models\CategoryBloc;
use Illuminate\Http\Request;
use Flash;
use Response;

class BlocFixController extends AppBaseController
{
    /** @var  BlocFixRepository */
    private $blocFixRepository;

    public function __construct(BlocFixRepository $blocFixRepo)
    {
        $this->blocFixRepository = $blocFixRepo;
    }

    /**
     * Display a listing of the BlocFix.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $blocFixes = $this->blocFixRepository->all();
        $blocFixes = BlocFix::orderBy('id', 'desc')->paginate(10);

        return view('bloc_fixes.index')
            ->with('blocFixes', $blocFixes);
    }

    /**
     * Show the form for creating a new BlocFix.
     *
     * @return Response
     */
    public function create()
    {
        $categoryBlocs = CategoryBloc::all();

        return view('bloc_fixes.create', compact('categoryBlocs'));
    }

    /**
     * Store a newly created BlocFix in storage.
     *
     * @param CreateBlocFixRequest $request
     *
     * @return Response
     */
    public function store(CreateBlocFixRequest $request)
    {
        $input = $request->all();

        $blocFix = $this->blocFixRepository->create($input);

        Flash::success('Bloc Fix saved successfully.');

        return redirect(route('blocFixes.index'));
    }

    /**
     * Display the specified BlocFix.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $blocFix = $this->blocFixRepository->find($id);

        if (empty($blocFix)) {
            Flash::error('Bloc Fix not found');

            return redirect(route('blocFixes.index'));
        }

        return view('bloc_fixes.show')->with('blocFix', $blocFix);
    }

    /**
     * Show the form for editing the specified BlocFix.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $blocFix = $this->blocFixRepository->find($id);

        if (empty($blocFix)) {
            Flash::error('Bloc Fix not found');

            return redirect(route('blocFixes.index'));
        }

        $categoryBlocs = CategoryBloc::all();

        return view('bloc_fixes.edit')
              ->with('blocFix', $blocFix)
              ->with('categoryBlocs', $categoryBlocs);
    }

    /**
     * Update the specified BlocFix in storage.
     *
     * @param int $id
     * @param UpdateBlocFixRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBlocFixRequest $request)
    {
        $blocFix = $this->blocFixRepository->find($id);

        if (empty($blocFix)) {
            Flash::error('Bloc Fix not found');

            return redirect(route('blocFixes.index'));
        }

        $blocFix = $this->blocFixRepository->update($request->all(), $id);

        Flash::success('Bloc Fix updated successfully.');

        return redirect(route('blocFixes.index'));
    }

    /**
     * Remove the specified BlocFix from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $blocFix = $this->blocFixRepository->find($id);

        if (empty($blocFix)) {
            Flash::error('Bloc Fix not found');

            return redirect(route('blocFixes.index'));
        }

        $this->blocFixRepository->delete($id);

        Flash::success('Bloc Fix deleted successfully.');

        return redirect(route('blocFixes.index'));
    }
}
