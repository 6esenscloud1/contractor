<?php

namespace App\Repositories;

use App\Models\ContactRequest;
use App\Repositories\BaseRepository;

/**
 * Class ContactRequestRepository
 * @package App\Repositories
 * @version March 15, 2021, 10:27 am UTC
*/

class ContactRequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ContactRequest::class;
    }
}
