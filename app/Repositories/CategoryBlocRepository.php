<?php

namespace App\Repositories;

use App\Models\CategoryBloc;
use App\Repositories\BaseRepository;

/**
 * Class CategoryBlocRepository
 * @package App\Repositories
 * @version March 15, 2021, 11:15 am UTC
*/

class CategoryBlocRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategoryBloc::class;
    }
}
