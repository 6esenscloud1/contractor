<?php

namespace App\Repositories;

use App\Models\BlocFix;
use App\Repositories\BaseRepository;

/**
 * Class BlocFixRepository
 * @package App\Repositories
 * @version March 15, 2021, 11:13 am UTC
*/

class BlocFixRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BlocFix::class;
    }
}
