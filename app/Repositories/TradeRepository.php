<?php

namespace App\Repositories;

use App\Models\Trade;
use App\Repositories\BaseRepository;

/**
 * Class TradeRepository
 * @package App\Repositories
 * @version March 12, 2021, 4:28 pm UTC
*/

class TradeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Trade::class;
    }
}
