<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class CategoryBloc
 * @package App\Models
 * @version March 15, 2021, 11:15 am UTC
 *
 */
class CategoryBloc extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'category_blocs';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'description',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
