<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Actuality
 * @package App\Models
 * @version March 12, 2021, 6:07 pm UTC
 *
 */
class Actuality extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'actualities';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'subtitle',
        'description',
        'image',
        'actuality_date',
        'medias'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'substitle' => 'string',
        'description' => 'string',
        'image' => 'string',
        'actuality_date' => 'date',
        'medias' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
