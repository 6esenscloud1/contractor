<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Team
 * @package App\Models
 * @version March 15, 2021, 10:35 am UTC
 *
 */
class Team extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'teams';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'post',
        'image',
        'link_linkedin',
        'link_facebook',
        'link_twitter',
        'medias',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'post' => 'string',
        'image' => 'string',
        'link_linkedin' => 'string',
        'link_facebook' => 'string',
        'medias' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
