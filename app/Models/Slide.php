<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Slide
 * @package App\Models
 * @version March 15, 2021, 10:44 am UTC
 *
 */
class Slide extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'slides';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'subtitle',
        'picture1',
        'picture2',
        'picture3',
        'page'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'subtitle' => 'string',
        'picture1' => 'string',
        'picture2' => 'string',
        'picture3' => 'string',
        'page' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
