<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class BlocFix
 * @package App\Models
 * @version March 15, 2021, 11:13 am UTC
 *
 */
class BlocFix extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'bloc_fixes';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'description',
        'title',
        'category_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'category_id' => 'integer',
        'title' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    public function getCategorieBlocAttribute()
    {
        $category = CategoryBloc::find($this->category_id);

        return $category;
    }

}
