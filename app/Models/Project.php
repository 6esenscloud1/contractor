<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Project
 * @package App\Models
 * @version March 15, 2021, 10:17 am UTC
 *
 */
class Project extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'projects';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'slug',
        'project_owner',
        'project_manager',
        'description',
        'start_of_work',
        'end_of_work',
        'image',
        'picture1',
        'picture2',
        'picture3',
        'picture4',
        'picture5',
        'picture6',
        'picture7',
        'medias',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'project_owner' => 'string',
        'project_manager' => 'string',
        'description' => 'string',
        'medias' => 'string',
        'image' => 'string',
        'picture1' => 'string',
        'picture2' => 'string',
        'picture3' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
